defmodule KingdomLifeWeb.SettlementController do
  use KingdomLifeWeb, :controller

  alias KingdomLife.Services.PDF, as: PDFService
  alias KingdomLife.Services.Settlement, as: SettlementService

  def index(conn, _params) do
    conn
    |> render("index.html")
  end

  def new(conn, params) do
    options =
      params
      |> to_options()

    settlement_pdf =
      options
      |> SettlementService.translate()
      |> SettlementService.build(options.version, options.campaign)
      |> PDFService.generate_pdf()

    conn
    |> put_resp_content_type("application/pdf")
    |> put_resp_header("content-disposition", "attachment; filename=html.pdf")
    |> Plug.Conn.send_resp(:ok, settlement_pdf)
    |> halt()
  end

  defp to_options(params) do
    {version, rest} = Map.pop(params["settlement"], "version")
    {campaign, monsters} = Map.pop(rest, "campaign")

    monsters
    |> Enum.map(fn {monster, add?} -> {String.to_atom(monster), add? == "true"} end)
    |> Enum.into(%{version: String.to_float(version), campaign: String.to_atom(campaign)})
  end
end
