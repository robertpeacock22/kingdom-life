defmodule KingdomLife.ResourceSet do
  @moduledoc """
  A Struct defining a set of Resources from a single source (basic, monster, strange, etc.)
  """

  use TypedStruct

  typedstruct do
    field :icon, String.t(), enforce: true
    field :name, String.t(), enforce: true
    field :resources, [Resource.t()], enforce: true
  end
end
