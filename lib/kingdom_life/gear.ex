defmodule KingdomLife.Gear do

  use TypedStruct

  typedstruct do
    field :name, String.t(), enforce: true
    field :count, integer(), enforce: true
    field :type, atom(), enforce: true
  end
end
