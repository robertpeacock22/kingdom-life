defmodule KingdomLife.Settlement do
  @moduledoc """
  A Struct representing a settlement.
  """
  alias KingdomLife.{Gear, GearSet, Innovation, Location, Monster, ResourceSet, Resource, StoryEvent}
  alias KingdomLife.Campaign.NoCampaign

  use TypedStruct

  typedstruct do
    field :campaign, Campaign.t(), enforce: true
    field :fighting_arts, [String.t()], enforce: true
    field :gear_sets, [GearSet.t()], enforce: true
    field :innovations, [Innovation.t()], enforce: true
    field :locations, [Location.t()], enforce: true
    field :name, String.t()
    field :principles, [String.t()], enforce: true
    field :monsters, [Monster.t()], enforce: true
    field :resources, [ResourceSet.t()], enforce: true
    field :story_events, [StoryEvent.t()], enforce: true
    field :version, atom(), enforce: true
    field :weapon_masteries, [Innovation.t()], enforce: true
    field :years, [LanternYear.t()], enforce: true
  end

  @basic_resources [
    %Resource{type: :basic, name: "???", tags: [:hide, :organ, :bone, :consumable]},
    %Resource{type: :basic, name: "Broken Lantern", tags: [:scrap]},
    %Resource{type: :basic, name: "Love Juice", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Monster Bone", tags: [:bone]},
    %Resource{type: :basic, name: "Monster Hide", tags: [:hide]},
    %Resource{type: :basic, name: "Monster Organ", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Scrap", tags: [:scrap]},
    %Resource{type: :basic, name: "Skull", tags: [:bone]}
  ]

  @strange_resources %{
    v1_0: [
      %Resource{type: :strange, name: "Fresh Acanthus", tags: [:herb]},
      %Resource{type: :strange, name: "Iron", tags: [:scrap, :iron]},
      %Resource{type: :strange, name: "Leather", tags: [:hide, :leather]},
      %Resource{type: :strange, name: "Perfect Crucible", tags: [:iron]}
    ],
    v1_5: [
      %Resource{type: :strange, name: "Black Lichen", tags: [:bone, :hide, :organ, :consumable, :other]},
      %Resource{type: :strange, name: "Cocoon Membrane", tags: [:organ, :other]},
      %Resource{type: :strange, name: "Lantern Tube", tags: [:organ, :scrap]}
    ]
  }

  @vermin_resources [
    %Resource{type: :vermin, name: "Crab Spider", tags: [:vermin, :consumable, :hide]},
    %Resource{type: :vermin, name: "Cyclops Fly", tags: [:vermin, :consumable]},
    %Resource{type: :vermin, name: "Hissing Cockroach", tags: [:vermin, :consumable]},
    %Resource{type: :vermin, name: "Lonely Ant", tags: [:vermin, :consumable]},
    %Resource{type: :vermin, name: "Nightmare Tick", tags: [:vermin, :consumable]},
    %Resource{type: :vermin, name: "Sword Beetle", tags: [:vermin, :consumable]},
  ]

  @innovations %{
    v1_0: [
      %Innovation{name: "Accept Darkness", type: :innovation, status: :unknown},
      %Innovation{name: "Ammonia", type: :innovation, status: :in_deck},
      %Innovation{name: "Barbaric", type: :innovation, status: :unknown},
      %Innovation{name: "Bed", type: :innovation, status: :unknown},
      %Innovation{name: "Bloodletting", type: :innovation, status: :unknown},
      %Innovation{name: "Cannibalize", type: :innovation, status: :unknown},
      %Innovation{name: "Collective Toil", type: :innovation, status: :unknown},
      %Innovation{name: "Cooking", type: :innovation, status: :unknown},
      %Innovation{name: "Drums", type: :innovation, status: :in_deck},
      %Innovation{name: "Face Painting", type: :innovation, status: :unknown},
      %Innovation{name: "Final Fighting Art", type: :innovation, status: :unknown},
      %Innovation{name: "Forbidden Dance", type: :innovation, status: :unknown},
      %Innovation{name: "Graves", type: :innovation, status: :unknown},
      %Innovation{name: "Heart Flute", type: :innovation, status: :unknown},
      %Innovation{name: "Hovel", type: :innovation, status: :in_deck},
      %Innovation{name: "Inner Lantern", type: :innovation, status: :in_deck},
      %Innovation{name: "Momento Mori", type: :innovation, status: :unknown},
      %Innovation{name: "Nightmare Training", type: :innovation, status: :unknown},
      %Innovation{name: "Paint", type: :innovation, status: :in_deck},
      %Innovation{name: "Partnership", type: :innovation, status: :unknown},
      %Innovation{name: "Pictograph", type: :innovation, status: :unknown},
      %Innovation{name: "Pottery", type: :innovation, status: :unknown},
      %Innovation{name: "Protect the Young", type: :innovation, status: :unknown},
      %Innovation{name: "Records", type: :innovation, status: :unknown},
      %Innovation{name: "Romantic", type: :innovation, status: :unknown},
      %Innovation{name: "Sacrifice", type: :innovation, status: :unknown},
      %Innovation{name: "Saga", type: :innovation, status: :unknown},
      %Innovation{name: "Scarification", type: :innovation, status: :unknown},
      %Innovation{name: "Scrap Smelting", type: :innovation, status: :unknown},
      %Innovation{name: "Sculpture", type: :innovation, status: :unknown},
      %Innovation{name: "Shrine", type: :innovation, status: :unknown},
      %Innovation{name: "Song of the Brave", type: :innovation, status: :unknown},
      %Innovation{name: "Storytelling", type: :innovation, status: :unknown},
      %Innovation{name: "Survival of the Fittest", type: :innovation, status: :unknown},
      %Innovation{name: "Symposium", type: :innovation, status: :in_deck},
      %Innovation{name: "Ultimate Weapon", type: :innovation, status: :unknown},
    ],
    v1_5: [
      %Innovation{name: "Destiny", type: :innovation, status: :unknown}
    ]
  }

  @weapon_masteries [
    %Innovation{name: "Axe", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Bow", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Club", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Dagger", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Fist & Tooth", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Grand Weapon", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Katana*", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Katar", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Scythe", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Shield", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Spear", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Sword", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Twilight Sword", type: :weapon_mastery, status: :unknown},
    %Innovation{name: "Whip", type: :weapon_mastery, status: :unknown}
  ]

  @gear_sets [
    %GearSet{
      name: "Starting Gear",
      gear: [
        # Starting Gear
        %Gear{count: 6, type: :starting ,name: "Cloth"},
        %Gear{count: 6, type: :starting, name: "Founding Stone"},
      ]
    },
    %GearSet{
      name: "Rare Gear",
      gear: [
        %Gear{count: 1, type: :rare, name: "Adventure Sword"}, # hunt event
        %Gear{count: 1, type: :rare, name: "Muramasa"}, # hunt event
        %Gear{count: 1, type: :rare, name: "Steel Shield"}, # hunt event
        %Gear{count: 1, type: :rare, name: "Steel Sword"}, # hunt event
        %Gear{count: 1, type: :rare, name: "Thunder Maul"}, # hunt event
        %Gear{count: 1, type: :rare, name: "Twilight Sword"}, # hunt event
      ]
    }
  ]

  def new(version) do
    locations = [
      Location.BoneSmith.details(version),
      Location.WeaponCrafter.details(version),
      Location.OrganGrinder.details(version),
      Location.StoneCircle.details(version),
      Location.Skinnery.details(version),
      Location.LeatherWorker.details(version),
      Location.MaskMaker.details(version)
    ]

    %__MODULE__{
      campaign: NoCampaign,
      fighting_arts: [],
      gear_sets: @gear_sets,
      innovations: [],
      locations: locations,
      principles: [],
      monsters: [],
      resources: resources(version),
      story_events: [],
      version: version,
      weapon_masteries: @weapon_masteries,
      years: []
    }
  end

  def innovations_for_version(1.0), do: @innovations.v1_0
  def innovations_for_version(1.5) do
    (@innovations.v1_0 ++ @innovations.v1_5)
  end

  defp resources(version) do
    basic = %ResourceSet{
      icon: "%",
      name: "Basic Resources",
      resources: @basic_resources
    }
    strange = %ResourceSet{
      icon: "h",
      name: "Strange Resources",
      resources: strange_resources(version)
    }

    vermin = %ResourceSet{
      icon: "u",
      name: "Vermin",
      resources: @vermin_resources
    }

    [basic, strange, vermin]
  end

  defp strange_resources(1.0), do: @strange_resources.v1_0
  defp strange_resources(1.5) do
    (@strange_resources.v1_0 ++ @strange_resources.v1_5)
    |> Enum.sort(&(&1.name < &2.name))
  end
end
