defmodule KingdomLife.StoryEvent do
  @moduledoc """
  A Struct defining a story event.

  Story events fall on certain Lantern Years, and are either always present
  or are a consequence of mixing in a particular Monster to your Settlement.
  """

  use TypedStruct

  typedstruct do
    field :title, String.t(), enforce: true
    field :type, atom(), enforce: true
    field :year_index, integer(), enforce: true
  end
end
