defmodule KingdomLife.Innovation do
  @moduledoc """
  A Struct defining innovations.

  Some innovations are part of the base game, while some are part
  of monster expansions. The innovations that are part of monster expansions
  are either mixed in along with the monster, or unique to one of the campaign
  types (Sunstalker has both types, for example).
  """

  use TypedStruct

  typedstruct do
    field :name, String.t(), enforce: true
    field :type, atom(), enforce: true
    field :status, atom(), enforce: true
  end
end
