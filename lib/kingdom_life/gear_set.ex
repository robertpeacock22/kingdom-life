defmodule KingdomLife.GearSet do
  @moduledoc """
  A Struct defining a set of Gear from a single source (core, Catarium, etc.)
  """

  use TypedStruct

  typedstruct do
    field :name, String.t(), enforce: true
    field :gear, [Gear.t()], enforce: true
  end
end
