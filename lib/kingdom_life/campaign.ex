defmodule KingdomLife.Campaign do
  @moduledoc """
  A Behaviour representing a campaign.
  """
  @callback name() :: String.t()
  @callback update_innovations(Settlement.t()) :: Settlement.t()
  @callback update_locations(Settlement.t()) :: Settlement.t()
  @callback add_story_events(Settlement.t()) :: Settlement.t()
  @callback check_monsters(Settlement.t()) :: Settlement.t()
end
