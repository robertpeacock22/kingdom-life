defmodule KingdomLife.Location.BoneSmith do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Bone Smith"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Bone Axe"},
          %Gear{count: 3, type: :gear, name: "Bone Blade"},
          %Gear{count: 3, type: :gear, name: "Bone Dagger"},
          %Gear{count: 3, type: :gear, name: "Bone Darts"},
          %Gear{count: 3, type: :gear, name: "Bone Pickaxe"},
          %Gear{count: 3, type: :gear, name: "Bone Sickle"},
          %Gear{count: 3, type: :gear, name: "Skull Helm"}
        ]
      }
    }
  end
end
