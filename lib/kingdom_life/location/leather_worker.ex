defmodule KingdomLife.Location.LeatherWorker do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Leather Worker"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Hunter Whip"},
          %Gear{count: 3, type: :gear, name: "Leather Boots"},
          %Gear{count: 3, type: :gear, name: "Leather Bracers"},
          %Gear{count: 3, type: :gear, name: "Leather Cuirass"},
          %Gear{count: 3, type: :gear, name: "Leather Mask"},
          %Gear{count: 3, type: :gear, name: "Leather Skirt"},
          %Gear{count: 3, type: :gear, name: "Round Leather Shield"}
        ]
      }
    }
  end
end
