defmodule KingdomLife.Location.WeaponCrafter do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Weapon Crafter"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Blood Sheath"},
          %Gear{count: 3, type: :gear, name: "Finger of God"},
          %Gear{count: 3, type: :gear, name: "Rainbow Katana"},
          %Gear{count: 3, type: :gear, name: "Whistling Mace"},
          %Gear{count: 3, type: :gear, name: "Skullcap Hammer"},
          %Gear{count: 3, type: :gear, name: "Scrap Dagger"},
          %Gear{count: 3, type: :gear, name: "Scrap Sword"},
          %Gear{count: 3, type: :gear, name: "Counterweighted Axe"},
          %Gear{count: 3, type: :gear, name: "Zanbato"}
        ]
      }
    }
  end
end
