defmodule KingdomLife.Location.DragonArmory do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Dragon Armory"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Blast Shield"},
          %Gear{count: 3, type: :gear, name: "Blast Sword"},
          %Gear{count: 3, type: :gear, name: "Blue Power Core"},
          %Gear{count: 2, type: :gear, name: "Dragon Belt"},
          %Gear{count: 3, type: :gear, name: "Dragon Bite Bolt"},
          %Gear{count: 2, type: :gear, name: "Dragon Boots"},
          %Gear{count: 3, type: :gear, name: "Dragon Chakram"},
          %Gear{count: 2, type: :gear, name: "Dragon Gloves"},
          %Gear{count: 2, type: :gear, name: "Dragon Mantle"},
          %Gear{count: 2, type: :gear, name: "Dragonskull Helm"},
          %Gear{count: 3, type: :gear, name: "Nuclear Knife"},
          %Gear{count: 3, type: :gear, name: "Nuclear Scythe"},
          %Gear{count: 3, type: :gear, name: "Red Power Core"},
          %Gear{count: 3, type: :gear, name: "Shielded Quiver"},
          %Gear{count: 3, type: :gear, name: "Talon Knife"}
        ]
      }
    }
  end
end
