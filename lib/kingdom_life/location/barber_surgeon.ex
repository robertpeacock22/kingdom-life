defmodule KingdomLife.Location.BarberSurgeon do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Barber Surgeon"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Almanac"},
          %Gear{count: 3, type: :gear, name: "Brain Mint"},
          %Gear{count: 3, type: :gear, name: "Bug Trap"},
          %Gear{count: 3, type: :gear, name: "Elder Earrings"},
          %Gear{count: 3, type: :gear, name: "First Aid Kit"},
          %Gear{count: 3, type: :gear, name: "Musk Bomb"},
          %Gear{count: 1, type: :gear, name: "Scavenger Kit"},
          %Gear{count: 3, type: :gear, name: "Speed Powder"}
        ]
      }
    }
  end
end
