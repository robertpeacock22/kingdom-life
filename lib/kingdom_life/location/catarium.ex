defmodule KingdomLife.Location.Catarium do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Catarium"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Cat Eye Circlet"},
          %Gear{count: 3, type: :gear, name: "Cat Fang Knife"},
          %Gear{count: 3, type: :gear, name: "Cat Gut Bow"},
          %Gear{count: 3, type: :gear, name: "Claw Head Arrow"},
          %Gear{count: 3, type: :gear, name: "Frenzy Drink"},
          %Gear{count: 3, type: :gear, name: "King Spear"},
          %Gear{count: 3, type: :gear, name: "Lion Beast Katar"},
          %Gear{count: 3, type: :gear, name: "Lion Headdress"},
          %Gear{count: 3, type: :gear, name: "Lion Skin Cloak"},
          %Gear{count: 3, type: :gear, name: "White Lion Boots"},
          %Gear{count: 3, type: :gear, name: "White Lion Coat"},
          %Gear{count: 3, type: :gear, name: "White Lion Gauntlet"},
          %Gear{count: 3, type: :gear, name: "White Lion Helm"},
          %Gear{count: 3, type: :gear, name: "White Lion Skirt"},
          %Gear{count: 3, type: :gear, name: "Whisker Harp"}
        ]
      }
    }
  end
end
