defmodule KingdomLife.Location.StoneCircle do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Stone Circle"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Beast Knuckle"},
          %Gear{count: 3, type: :gear, name: "Blood Paint"},
          %Gear{count: 3, type: :gear, name: "Blue Charm"},
          %Gear{count: 3, type: :gear, name: "Bone Earrings"},
          %Gear{count: 3, type: :gear, name: "Boss Mehndi"},
          %Gear{count: 3, type: :gear, name: "Green Charm"},
          %Gear{count: 3, type: :gear, name: "Lance of Longinus"},
          %Gear{count: 3, type: :gear, name: "Red Charm"},
          %Gear{count: 3, type: :gear, name: "Screaming Bracers"},
          %Gear{count: 3, type: :gear, name: "Screaming Coat"},
          %Gear{count: 3, type: :gear, name: "Screaming Horns"},
          %Gear{count: 3, type: :gear, name: "Screaming Leg Warmers"},
          %Gear{count: 3, type: :gear, name: "Screaming Skirt"}
        ]
      }
    }
  end
end
