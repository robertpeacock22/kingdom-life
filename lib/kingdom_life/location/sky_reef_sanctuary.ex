defmodule KingdomLife.Location.SkyReefSanctuary do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Sky Reef Sanctuary"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Cycloid Scale Hood"},
          %Gear{count: 3, type: :gear, name: "Cycloid Scale Jacket"},
          %Gear{count: 3, type: :gear, name: "Cycloid Scale Shoes"},
          %Gear{count: 3, type: :gear, name: "Cycloid Scale Skirt"},
          %Gear{count: 3, type: :gear, name: "Cycloid Scale Sleeves"},
          %Gear{count: 3, type: :gear, name: "Denticle Axe"},
          %Gear{count: 3, type: :gear, name: "Ink Blood Bow"},
          %Gear{count: 3, type: :gear, name: "Ink Sword"},
          %Gear{count: 3, type: :gear, name: "Quiver & Sun String"},
          %Gear{count: 3, type: :gear, name: "Shadow Salvia Shawl"},
          %Gear{count: 3, type: :gear, name: "Skleaver"},
          %Gear{count: 3, type: :gear, name: "Sky Harpoon"},
          %Gear{count: 3, type: :gear, name: "Sun Lure and Hook"},
          %Gear{count: 3, type: :gear, name: "Sunshark Arrows"},
          %Gear{count: 3, type: :gear, name: "Sunshark Bow"},
          %Gear{count: 3, type: :gear, name: "Sunspot Dart"},
          %Gear{count: 3, type: :gear, name: "Sunspot Lantern"}
        ]
      }
    }
  end
end
