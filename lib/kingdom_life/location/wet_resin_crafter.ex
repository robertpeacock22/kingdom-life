defmodule KingdomLife.Location.WetResinCrafter do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Wet Resin Crafter"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 1, type: :gear, name: "DBK Errant Badge"},
          %Gear{count: 1, type: :gear, name: "The Beetle Bomb"},
          %Gear{count: 3, type: :gear, name: "Calcified Greaves"},
          %Gear{count: 3, type: :gear, name: "Calcified Juggernaut Blade"},
          %Gear{count: 3, type: :gear, name: "Calcified Shoulder Pads"},
          %Gear{count: 3, type: :gear, name: "Calcified Zanbato"},
          %Gear{count: 3, type: :gear, name: "Century Greaves"},
          %Gear{count: 3, type: :gear, name: "Century Shoulder Pads"},
          %Gear{count: 3, type: :gear, name: "Rainbow Wing Belt"},
          %Gear{count: 3, type: :gear, name: "Regenerating Blade"},
          %Gear{count: 3, type: :gear, name: "Rubber Bone Harness"},
          %Gear{count: 3, type: :gear, name: "Scarab Circlet"},
          %Gear{count: 3, type: :gear, name: "Seasoned Monster Meat"},
          %Gear{count: 3, type: :gear, name: "Zanbato"},
          %Gear{count: 4, type: :gear, name: "Calcified Digging Claw"},
          %Gear{count: 4, type: :gear, name: "Digging Claw"}
        ]
      }
    }
  end
end
