defmodule KingdomLife.Location.SilkMill do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Silk Mill"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 1, type: :gear, name: "Blue Ring"},
          %Gear{count: 1, type: :gear, name: "Green Ring"},
          %Gear{count: 1, type: :gear, name: "Red Ring"},
          %Gear{count: 1, type: :gear, name: "Silk Body Suit"},
          %Gear{count: 2, type: :gear, name: "Silk Boots"},
          %Gear{count: 2, type: :gear, name: "Silk Robes"},
          %Gear{count: 2, type: :gear, name: "Silk Sash"},
          %Gear{count: 2, type: :gear, name: "Silk Turban"},
          %Gear{count: 2, type: :gear, name: "Silk Wraps"},
          %Gear{count: 3, type: :gear, name: "Amber Edge"},
          %Gear{count: 3, type: :gear, name: "Amber Poleaxe"},
          %Gear{count: 3, type: :gear, name: "Hooded Scrap Katar"},
          %Gear{count: 3, type: :gear, name: "Silk Bomb"},
          %Gear{count: 3, type: :gear, name: "Silk Whip"},
          %Gear{count: 3, type: :gear, name: "Throwing Knife"}
        ]
      }
    }
  end
end
