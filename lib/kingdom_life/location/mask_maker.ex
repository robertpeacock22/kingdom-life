defmodule KingdomLife.Location.MaskMaker do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Mask Maker"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
        %Gear{count: 1, type: :gear, name: "Antelope Mask"},
        %Gear{count: 1, type: :gear, name: "Death Mask"},
        %Gear{count: 1, type: :gear, name: "God Mask"},
        %Gear{count: 1, type: :gear, name: "Man Mask"},
        %Gear{count: 1, type: :gear, name: "Phoenix Mask"},
        %Gear{count: 1, type: :gear, name: "White Lion Mask"}
        ]
      }
    }
  end
end
