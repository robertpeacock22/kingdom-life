defmodule KingdomLife.Location.Gormery do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Gormery"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Acid Tooth Dagger"},
          %Gear{count: 3, type: :gear, name: "Armor Spikes"},
          %Gear{count: 3, type: :gear, name: "Black Sword"},
          %Gear{count: 3, type: :gear, name: "Gaxe"},
          %Gear{count: 2, type: :gear, name: "Gorment Boots"},
          %Gear{count: 2, type: :gear, name: "Gorment Mask"},
          %Gear{count: 2, type: :gear, name: "Gorment Sleeves"},
          %Gear{count: 2, type: :gear, name: "Gorment Suit"},
          %Gear{count: 3, type: :gear, name: "Gorn"},
          %Gear{count: 3, type: :gear, name: "Greater Gaxe"},
          %Gear{count: 3, type: :gear, name: "Knuckle Shield"},
          %Gear{count: 3, type: :gear, name: "Pulse Lantern"},
          %Gear{count: 2, type: :gear, name: "Regeneration Suit"},
          %Gear{count: 3, type: :gear, name: "Rib Blade"},
          %Gear{count: 3, type: :gear, name: "Riot Mace"}
        ]
      }
    }
  end
end
