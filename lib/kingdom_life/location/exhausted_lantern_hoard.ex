defmodule KingdomLife.Location.ExhaustedLanternHoard do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Exhausted Lantern Hoard"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Oxidized Beacon Shield"},
          %Gear{count: 4, type: :gear, name: "Oxidized Lantern Dagger"},
          %Gear{count: 3, type: :gear, name: "Oxidized Lantern Glaive"},
          %Gear{count: 3, type: :gear, name: "Oxidized Lantern Helm"},
          %Gear{count: 3, type: :gear, name: "Oxidized Lantern Sword"},
          %Gear{count: 3, type: :gear, name: "Oxidized Ring Whip"},
          %Gear{count: 3, type: :gear, name: "Survivors Lantern"}
        ]
      }
    }
  end
end
