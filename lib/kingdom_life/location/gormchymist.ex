defmodule KingdomLife.Location.Gormchymist do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Gormchymist"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Healing Potion"},
          %Gear{count: 3, type: :gear, name: "Life Elixir"},
          %Gear{count: 3, type: :gear, name: "Power Potion"},
          %Gear{count: 3, type: :gear, name: "Steadfast Potion"},
          %Gear{count: 3, type: :gear, name: "Wisdom Potion"}
        ]
      }
    }
  end
end
