defmodule KingdomLife.Location.Skinnery do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Skinnery"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
        %Gear{count: 3, type: :gear, name: "Bandages"},
        %Gear{count: 3, type: :gear, name: "Rawhide Boots"},
        %Gear{count: 3, type: :gear, name: "Rawhide Drum"},
        %Gear{count: 3, type: :gear, name: "Rawhide Gloves"},
        %Gear{count: 3, type: :gear, name: "Rawhide Headband"},
        %Gear{count: 3, type: :gear, name: "Rawhide Pants"},
        %Gear{count: 3, type: :gear, name: "Rawhide Vest"},
        %Gear{count: 3, type: :gear, name: "Rawhide Whip"}
        ]
      }
    }
  end
end
