defmodule KingdomLife.Location.Plumery do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Plumery"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          # Plumery
          %Gear{count: 3, type: :gear, name: "Arc Bow"},
          %Gear{count: 3, type: :gear, name: "Bird Bread"},
          %Gear{count: 3, type: :gear, name: "Bloom Sphere"},
          %Gear{count: 3, type: :gear, name: "Crest Crown"},
          %Gear{count: 3, type: :gear, name: "Feather Mantle"},
          %Gear{count: 3, type: :gear, name: "Feather Shield"},
          %Gear{count: 3, type: :gear, name: "Hollow Sword"},
          %Gear{count: 3, type: :gear, name: "Hollowpoint Arrow"},
          %Gear{count: 3, type: :gear, name: "Hours Ring"},
          %Gear{count: 3, type: :gear, name: "Phoenix Faulds"},
          %Gear{count: 3, type: :gear, name: "Phoenix Gauntlet"},
          %Gear{count: 3, type: :gear, name: "Phoenix Greaves"},
          %Gear{count: 3, type: :gear, name: "Phoenix Helm"},
          %Gear{count: 3, type: :gear, name: "Phoenix Plackart"},
          %Gear{count: 3, type: :gear, name: "Sonic Tomahawk"}
        ]
      }
    }
  end
end
