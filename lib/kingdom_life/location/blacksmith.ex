defmodule KingdomLife.Location.Blacksmith do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Blacksmith"

  @impl Location
  def details(_) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: [
          %Gear{count: 3, type: :gear, name: "Beacon Shield"},
          %Gear{count: 3, type: :gear, name: "Dragon Slayer"},
          %Gear{count: 3, type: :gear, name: "Lantern Cuirass"},
          %Gear{count: 3, type: :gear, name: "Lantern Dagger"},
          %Gear{count: 3, type: :gear, name: "Lantern Gauntlets"},
          %Gear{count: 3, type: :gear, name: "Lantern Glaive"},
          %Gear{count: 3, type: :gear, name: "Lantern Greaves"},
          %Gear{count: 3, type: :gear, name: "Lantern Helm"},
          %Gear{count: 3, type: :gear, name: "Lantern Mail"},
          %Gear{count: 3, type: :gear, name: "Lantern Sword"},
          %Gear{count: 3, type: :gear, name: "Perfect Slayer"},
          %Gear{count: 3, type: :gear, name: "Ring Whip"},
          %Gear{count: 3, type: :gear, name: "Scrap Shield"}
        ]
      }
    }
  end
end
