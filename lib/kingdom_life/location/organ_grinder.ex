defmodule KingdomLife.Location.OrganGrinder do
  alias KingdomLife.{Gear, GearSet, Location}
  @behaviour Location

  @name "Organ Grinder"

  @gear %{
    v1_0: [
      %Gear{count: 3, type: :gear, name: "Dried Acanthus"},
      %Gear{count: 3, type: :gear, name: "Fecal Salve"},
      %Gear{count: 3, type: :gear, name: "Lucky Charm"},
      %Gear{count: 3, type: :gear, name: "Monster Grease"},
      %Gear{count: 3, type: :gear, name: "Monster Tooth Necklace"}
    ],
    v1_5: [
      %Gear{count: 3, type: :gear, name: "Stone Noses"}
    ]
  }

  @impl Location
  def details(version) do
    %Location{
      name: @name,
      status: :unclaimed,
      gear_set: %GearSet{
        name: @name,
        gear: gear(version)
      }
    }
  end

  defp gear(1.0), do: @gear.v1_0
  defp gear(1.5) do
    (@gear.v1_0 ++ @gear.v1_5)
    |> Enum.sort(&(&1.name < &2.name))
  end
end
