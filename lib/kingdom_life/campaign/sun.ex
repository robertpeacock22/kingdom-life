defmodule KingdomLife.Campaign.Sun do
  alias KingdomLife.{Campaign, Gear, GearSet, Innovation, Location, Settlement, StoryEvent}
  alias KingdomLife.Services.Settlement, as: SettlementService

  @people_of_the_sun_innovations [
    # New Innovations
    %Innovation{name: "Aquarobics", type: :innovation, status: :unknown},
    %Innovation{name: "Filleting Table", type: :innovation, status: :unknown},
    %Innovation{name: "Hands of the Sun", type: :innovation, status: :in_deck},
    %Innovation{name: "Sauna Shrine", type: :innovation, status: :unknown},
    %Innovation{name: "Sun Language", type: :innovation, status: :known},
    %Innovation{name: "Umbilical Bank", type: :innovation, status: :unknown},
    # Dragon King disallows
    %Innovation{name: "Lantern Oven", type: :innovation, status: :unknown},
    %Innovation{name: "Family", type: :innovation, status: :unknown},
    %Innovation{name: "Clan of Death", type: :innovation, status: :unknown}
  ]

  @people_of_the_sun_locations [
    %Location{name: "The Sun", status: :owned, gear_set: []},
    %Location{
      name: "Sacred Pool ■1 ☐2 ☐3",
      status: :owned,
      gear_set: %GearSet{
        name: "Sacred Pool",
        gear: [
          %Gear{count: 2, type: :rare, name: "Sun Vestments"},
          %Gear{count: 1, type: :rare, name: "Sunring Bow"},
          %Gear{count: 1, type: :rare, name: "Apostle Crown"},
          %Gear{count: 1, type: :rare, name: "Prism Mace"}
        ]
      }
    }
  ]

  @people_of_the_sun_story_events [
    %StoryEvent{year_index: 1, title: "The Pool and the Sun", type: :story},
    %StoryEvent{year_index: 4, title: "Sun Dipping", type: :story},
    %StoryEvent{year_index: 5, title: "The Great Sky Gift", type: :story},
    %StoryEvent{year_index: 10, title: "Birth of Color", type: :story},
    %StoryEvent{year_index: 11, title: "Principle: Conviction", type: :story},
    %StoryEvent{year_index: 12, title: "Sun Dipping", type: :story},
    %StoryEvent{year_index: 13, title: "The Great Sky Gift", type: :story},
    %StoryEvent{year_index: 19, title: "Sun Dipping", type: :story},
    %StoryEvent{year_index: 20, title: "Final Gift", type: :story},
    %StoryEvent{year_index: 25, title: "Nemesis - The Great Devourer", type: :story},
  ]

  @prohibited_monsters [
    "The Watcher"
  ]

  @behaviour Campaign
  def name, do: "People of the Sun"

  def update_innovations(%Settlement{innovations: innovations} = settlement) do
    updated_innovations =
      innovations
      |> Kernel.++(@people_of_the_sun_innovations)
      |> Enum.sort(&(&1.name < &2.name))

    %Settlement{settlement | innovations: updated_innovations}
  end

  def update_locations(%Settlement{locations: locations} = settlement) do
    updated_locations = @people_of_the_sun_locations ++ locations

    %Settlement{settlement | locations: updated_locations}
  end

  def add_story_events(%Settlement{years: years} = settlement) do
    updated_years =
      @people_of_the_sun_story_events
      |> Enum.reduce(years, &SettlementService.add_story_event_to_years/2)

    %Settlement{settlement | years: updated_years}
  end

  def check_monsters(%Settlement{monsters: monsters} = settlement) do
    pruned_monsters =
      monsters
      |> Enum.reject(fn monster -> monster.name in @prohibited_monsters end)

    pruned_monsters |> Enum.map(fn monster -> IO.inspect(monster.name) end)

    %Settlement{settlement | monsters: pruned_monsters}
  end
end
