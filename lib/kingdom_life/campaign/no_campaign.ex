defmodule KingdomLife.Campaign.NoCampaign do
  alias KingdomLife.Campaign

  @behaviour Campaign

  def name(), do: campaign_exception()
  def update_innovations(_), do: campaign_exception()
  def update_locations(_), do: campaign_exception()
  def add_story_events(_), do: campaign_exception()
  def check_monsters(_), do: campaign_exception()

  defp campaign_exception(), do: raise "No Campaign has been set"
end
