defmodule KingdomLife.Campaign.Lantern do
  alias KingdomLife.{Campaign, Innovation, Location, Settlement, StoryEvent}
  alias KingdomLife.Services.Settlement, as: SettlementService

  @behaviour Campaign

  @people_of_the_lantern_innovations [
    # Dragon King/Sunstalker disallows
    %Innovation{name: "Language", type: :innovation, status: :known},
    # Dragon King disallows
    %Innovation{name: "Lantern Oven", type: :innovation, status: :unknown},
    %Innovation{name: "Family", type: :innovation, status: :unknown},
    %Innovation{name: "Clan of Death", type: :innovation, status: :unknown},
  ]

  @people_of_the_lantern_locations [
    %Location{name: "Lantern Hoard", status: :owned, gear_set: []}
  ]

  @people_of_the_lantern_story_events %{
    base: [
      %StoryEvent{year_index: 1, title: "Returning Survivors", type: :story},
      %StoryEvent{year_index: 5, title: "Hands of Heat", type: :story},
      %StoryEvent{year_index: 12, title: "Principle: Conviction", type: :story}
    ],
    v1_0: [
      %StoryEvent{year_index: 16, title: "Nemesis Encounter", type: :nemesis},
      %StoryEvent{year_index: 19, title: "Nemesis Encounter", type: :nemesis},
      %StoryEvent{year_index: 23, title: "Nemesis - Level 3", type: :nemesis}
    ]
  }

  def name, do: "People of the Lantern"

  def update_innovations(%Settlement{innovations: innovations} = settlement) do
    updated_innovations =
      innovations
      |> Kernel.++(@people_of_the_lantern_innovations)
      |> Enum.sort(&(&1.name < &2.name))

    %Settlement{settlement | innovations: updated_innovations}
  end

  def update_locations(%Settlement{locations: locations} = settlement) do
    updated_locations = @people_of_the_lantern_locations ++ locations

    %Settlement{settlement | locations: updated_locations}
  end

  def add_story_events(%Settlement{version: version, years: years} = settlement) do
    updated_years =
      version
      |> story_events()
      |> Enum.reduce(years, &SettlementService.add_story_event_to_years/2)

    %Settlement{settlement | years: updated_years}
  end

  def check_monsters(%Settlement{} = settlement), do: settlement

  # v1.0 gets some unspecified Nemesis Encounters
  defp story_events(1.0), do: @people_of_the_lantern_story_events.base ++ @people_of_the_lantern_story_events.v1_0
  # v1.5 specified its Nemesis Encounters. These will come from the Nemesis Monsters themselves.
  defp story_events(1.5), do: @people_of_the_lantern_story_events.base
end
