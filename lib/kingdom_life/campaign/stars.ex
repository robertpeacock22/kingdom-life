defmodule KingdomLife.Campaign.Stars do
  alias KingdomLife.{Campaign, Innovation, Location, Settlement, StoryEvent}
  alias KingdomLife.Services.Settlement, as: SettlementService

  @people_of_the_stars_innovations [
    %Innovation{name: "Arena", type: :innovation, status: :unknown},
    %Innovation{name: "Bloodline", type: :innovation, status: :unknown},
    %Innovation{name: "Dragon Speech", type: :innovation, status: :known},
    %Innovation{name: "Empire", type: :innovation, status: :unknown},
    %Innovation{name: "Radiating Orb", type: :innovation, status: :unknown}
  ]

  @people_of_the_stars_locations [
    %Location{name: "The Throne", status: :owned, gear_set: []}
  ]

  @people_of_the_stars_story_events [
    %StoryEvent{year_index: 1, title: "The Foundlings", type: :story},
    %StoryEvent{year_index: 4, title: "Nemesis - Dragon King Human Lvl 1", type: :nemesis},
    %StoryEvent{year_index: 5, title: "Midnight's Children", type: :story},
    %StoryEvent{year_index: 9, title: "Nemesis - Dragon King Human Lvl 2", type: :nemesis},
    %StoryEvent{year_index: 10, title: "Unveil the Sky", type: :story},
    %StoryEvent{year_index: 12, title: "Principle: Conviction", type: :story},
    %StoryEvent{year_index: 13, title: "Nemesis - Butcher Lvl 2", type: :nemesis},
    %StoryEvent{year_index: 16, title: "Nemesis - Lvl 2", type: :nemesis},
    %StoryEvent{year_index: 19, title: "Nemesis - Dragon King Human Lvl 3", type: :nemesis},
    %StoryEvent{year_index: 20, title: "The Dragon's Tomb", type: :story},
    %StoryEvent{year_index: 23, title: "Nemesis Encounter Lvl 3", type: :nemesis},
    %StoryEvent{year_index: 25, title: "Nemesis - Death of the Dragon King", type: :nemesis}
  ]

  @prohibited_monsters [
    "The Watcher"
  ]

  @behaviour Campaign
  def name, do: "People of the Stars"

  def update_innovations(%Settlement{innovations: innovations} = settlement) do
    updated_innovations =
      innovations
      |> Kernel.++(@people_of_the_stars_innovations)
      |> Enum.sort(&(&1.name < &2.name))

    %Settlement{settlement | innovations: updated_innovations}
  end

  def update_locations(%Settlement{locations: locations} = settlement) do
    updated_locations = @people_of_the_stars_locations ++ locations

    %Settlement{settlement | locations: updated_locations}
  end

  def add_story_events(%Settlement{years: years} = settlement) do
    updated_years =
      @people_of_the_stars_story_events
      |> Enum.reduce(years, &SettlementService.add_story_event_to_years/2)

    %Settlement{settlement | years: updated_years}
  end

  def check_monsters(%Settlement{monsters: monsters} = settlement) do
    pruned_monsters =
      monsters
      |> Enum.reject(fn monster -> monster.name in @prohibited_monsters end)

    pruned_monsters |> Enum.map(fn monster -> IO.inspect(monster.name) end)

    %Settlement{settlement | monsters: pruned_monsters}
  end
end
