defmodule KingdomLife.Location do
  @moduledoc """
  A Struct defining settlement locations.
  """
  alias KingdomLife.GearSet

  use TypedStruct

  @callback details(float()) :: Location.t()

  typedstruct do
    field :name, String.t(), enforce: true
    field :gear_set, GearSet.t(), enforce: true
    field :status, atom(), enforce: true
  end
end
