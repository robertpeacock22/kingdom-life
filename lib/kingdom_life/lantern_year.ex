defmodule KingdomLife.LanternYear do
  @moduledoc """
  A Struct defining lantern years.
  """

  use TypedStruct

  typedstruct do
    field :index, integer(), enforce: true
    field :story_events, [String.t()], enforce: true
    field :nemesis_encounters, [ResourceSet.t()], enforce: true
  end

  def new(year_index) do
    %__MODULE__{
      index: year_index,
      story_events: [],
      nemesis_encounters: []
    }
  end
end
