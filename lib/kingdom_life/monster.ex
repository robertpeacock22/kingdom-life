defmodule KingdomLife.Monster do
  @moduledoc """
  A Struct defining monsters.

  These are the eponymous Monsters of Kingdom Death: Monster. Each monster adds
  Monster Resource sets to the overall list of resources that a Settlement will
  need to keep track of. By extension, these new Monster Resources add new pieces
  of Gear that players will likely end up crafting.
  """

  use TypedStruct

  alias KingdomLife.{GearSet, Innovation, ResourceSet, Settlement, StoryEvent}

  typedstruct do
    field :gear_sets, [GearSet.t()], enforce: true
    field :icon, String.t(), enforce: true
    field :innovations, [Innovation.t()], enforce: true
    field :locations, Location.t(), enforce: true
    field :name, String.t(), enforce: true
    field :resource_set, ResourceSet.t(), enforce: true
    field :settlement_modifier, (Settlement.t() -> Settlement.t()), enforce: true
    field :story_events, [StoryEvent.t()], enforce: true
    field :type, atom(), enforce: true
  end

  @callback gear_sets(float()) :: [GearSet.t()]
  @callback icon() :: String.t()
  @callback innovations() :: [Innovation.t()]
  @callback locations(float()) :: [Location.t()]
  @callback name() :: String.t()
  @callback resources() :: [Resource.t()]
  @callback story_events(Settlement.t()) :: [StoryEvent.t()]
  @callback strange_resources() :: [Resource.t()]
  @callback settlement_modifier() :: Settlement.t()
  @callback type() :: atom()

  def noop(%Settlement{} = settlement), do: settlement

  def details(monster, %Settlement{version: version} = settlement) do
    gear_sets = monster.gear_sets(version)
    icon = monster.icon()
    innovations = monster.innovations()
    locations = monster.locations(version)
    name = monster.name()
    resources = monster.resources()
    settlement_modifier = monster.settlement_modifier()
    story_events = monster.story_events(settlement)
    strange_resources = monster.strange_resources()
    type = monster.type()

    resource_set = %ResourceSet{
      icon: icon,
      name: name,
      resources: strange_resources ++ resources
    }

    %__MODULE__{
      gear_sets: gear_sets,
      icon: icon,
      innovations: innovations,
      locations: locations,
      name: name,
      resource_set: resource_set,
      settlement_modifier: settlement_modifier,
      story_events: story_events,
      type: type
    }
  end
end
