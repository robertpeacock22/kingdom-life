defmodule KingdomLife.Monster.Knight.GoldSmoke do
  alias KingdomLife.{Monster, StoryEvent}

  @behaviour Monster

  @icon "0"

  @innovations []

  @name "Gold Smoke Knight"

  @resources []

  @strange_resources []

  @story_events [
    %StoryEvent{year_index: 30, title: "Nemesis - Gold Smoke Knight", type: :nemesis}
  ]

  @gear []

  @monster_type :nemesis

  @impl Monster
  def gear_sets(_), do: @gear
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(_), do: []
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
