defmodule KingdomLife.Monster.Knight.Flower do
  alias KingdomLife.{Gear, GearSet, Monster, Resource, StoryEvent}

  @behaviour Monster

  @icon "m"

  @innovations []

  @name "Flower Knight"

  @resources [
    %Resource{type: :basic, name: "Lantern Bloom", tags: [:flower, :hide, :perishable]},
    %Resource{type: :basic, name: "Lantern Bud", tags: [:flower, :scrap, :perishable]},
    %Resource{type: :basic, name: "Osseous Bloom", tags: [:flower, :bone, :perishable]},
    %Resource{type: :basic, name: "Sighing Bloom", tags: [:flower, :organ, :perishable]},
    %Resource{type: :basic, name: "Warbling Bloom", tags: [:flower, :hide, :perishable]}
  ]

  @strange_resources []

  @story_events [
    %StoryEvent{year_index: 5, title: "A Crone's Tale", type: :story}
  ]

  @rare_gear [
    %Gear{count: 3, type: :rare, name: "Flower Knight Helm"},
    %Gear{count: 3, type: :rare, name: "Replica Flower Sword"},
    %Gear{count: 3, type: :rare, name: "Sleeping Virus Flower"}
  ]
  @gear [
    %Gear{count: 1, type: :gear, name: "Flower Knight Badge"},
    %Gear{count: 3, type: :gear, name: "Satchel"},
    %Gear{count: 3, type: :gear, name: "Vespertine Arrow"},
    %Gear{count: 3, type: :gear, name: "Vespertine Bow"},
    %Gear{count: 1, type: :gear, name: "Vespertine Cello"},
    %Gear{count: 3, type: :gear, name: "Vespertine Foil"}
  ]

  @monster_type :quarry

  @impl Monster
  def gear_sets(_) do
    [
      %GearSet{
        name: "Rare Gear",
        gear: @rare_gear
      },
      %GearSet{
        name: "Sense Memory",
        gear: @gear
      }
    ]
  end
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(_), do: []
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
