defmodule KingdomLife.Monster.Knight.Lion do
  alias KingdomLife.{Gear, GearSet, Innovation, Monster, StoryEvent}

  @behaviour Monster

  @icon "-"

  @innovations [
    %Innovation{name: "Stoic Statue", type: :innovation, status: :unknown}
  ]

  @name "Lion Knight"

  @resources []

  @strange_resources []

  @story_events [
    %StoryEvent{year_index: 6, title: "An Uninvited Guest", type: :story}
  ]

  @rare_gear [
    %Gear{count: 3, type: :rare, name: "Hideous Disguise"},
    %Gear{count: 1, type: :rare, name: "Lion Knight Badge"},
    %Gear{count: 1, type: :rare, name: "Lion Knight’s Left Claw"},
    %Gear{count: 1, type: :rare, name: "Lion Knight’s Right Claw"}
  ]

  @monster_type :nemesis

  @impl Monster
  def gear_sets(_) do
    [
      %GearSet{
        name: "Rare Gear",
        gear: @rare_gear
      }
    ]
  end
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(_), do: []
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
