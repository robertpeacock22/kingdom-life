defmodule KingdomLife.Monster.Knight.DungBeetle do
  alias KingdomLife.{Gear, GearSet, Innovation, Monster, Resource, StoryEvent}
  alias KingdomLife.Location.WetResinCrafter

  @behaviour Monster

  @icon "9"

  @innovations [
    %Innovation{name: "Round Stone Training", type: :innovation, status: :unknown},
    %Innovation{name: "Subterranean Agriculture", type: :innovation, status: :unknown}
  ]

  @name "Dung Beetle Knight"

  @resources [
    %Resource{type: :basic, name: "Beetle Horn", tags: [:bone]},
    %Resource{type: :basic, name: "Century Fingernails", tags: [:bone]},
    %Resource{type: :basic, name: "Century Shell", tags: [:hide, :iron]},
    %Resource{type: :basic, name: "Compound Eye", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Elytra", tags: [:bone, :hide, :organ]},
    %Resource{type: :basic, name: "Scarab Shell", tags: [:hide]},
    %Resource{type: :basic, name: "Scarab Wing", tags: [:organ]},
    %Resource{type: :basic, name: "Underplate Fungus", tags: [:herb, :hide, :consumable]}
  ]

  @strange_resources [
    %Resource{type: :strange, name: "Preserved Caustic Dung", tags: [:organ, :consumable, :dung]},
    %Resource{type: :strange, name: "Scell", tags: [:organ, :consumable]}
  ]

  @story_events [
    %StoryEvent{year_index: 8, title: "Rumbling in the Dark", type: :story}
  ]

  @rare_gear [
    %Gear{count: 1, type: :rare, name: "Hidden Crimson Jewel"},
    %Gear{count: 3, type: :rare, name: "Trash Crown"},
  ]

  @monster_type :quarry

  @impl Monster
  def gear_sets(_) do
    [
      %GearSet{
        name: "Rare Gear",
        gear: @rare_gear
      }
    ]
  end
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(version), do: [WetResinCrafter.details(version)]
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
