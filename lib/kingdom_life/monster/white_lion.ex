defmodule KingdomLife.Monster.WhiteLion do
  alias KingdomLife.{Monster, Resource}
  alias KingdomLife.Location.Catarium

  @behaviour Monster

  @gear []

  @icon "+"

  @innovations []

  @name "White Lion"

  @resources [
    %Resource{type: :basic, name: "Curious Hand", tags: [:hide]},
    %Resource{type: :basic, name: "Eye of Cat", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Golden Whiskers", tags: [:organ]},
    %Resource{type: :basic, name: "Great Cat Bones", tags: [:bone]},
    %Resource{type: :basic, name: "Lion Claw", tags: [:bone]},
    %Resource{type: :basic, name: "Lion Tail", tags: [:hide]},
    %Resource{type: :basic, name: "Lion Testes", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Shimmering Mane", tags: [:hide]},
    %Resource{type: :basic, name: "Sinew", tags: [:organ]},
    %Resource{type: :basic, name: "White Fur", tags: [:hide]}
  ]

  @story_events []

  @strange_resources [
    %Resource{type: :strange, name: "Elder Cat Teeth", tags: [:bone]}
  ]

  @monster_type :quarry

  @impl Monster
  def gear_sets(_), do: @gear
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(version), do: [
    Catarium.details(version)
  ]
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
