defmodule KingdomLife.Monster.ScreamingAntelope do
  alias KingdomLife.{Monster, Resource, StoryEvent}
  alias KingdomLife.Location.BarberSurgeon

  @behaviour Monster

  @gear []

  @icon "Y"

  @innovations []

  @name "Screaming Antelope"

  @resources [
    %Resource{type: :basic, name: "Beast Steak", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Bladder", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Large Flat Tooth", tags: [:bone]},
    %Resource{type: :basic, name: "Muscly Gums", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Pelt", tags: [:hide]},
    %Resource{type: :basic, name: "Screaming Brain", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Shank Bones", tags: [:bone]},
    %Resource{type: :basic, name: "Spiral Horn", tags: [:bone]}
  ]

  @story_events [
    %StoryEvent{year_index: 2, title: "Endless Screams", type: :story}
  ]

  @strange_resources [
    %Resource{type: :strange, name: "Legendary Horns", tags: [:bone, :scrap]},
    %Resource{type: :strange, name: "Second Heart", tags: [:organ, :bone]}
  ]

  @monster_type :quarry

  @impl Monster
  def gear_sets(_), do: @gear
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(version), do: [BarberSurgeon.details(version)]
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
