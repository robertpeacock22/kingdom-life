defmodule KingdomLife.Monster.Phoenix do
  alias KingdomLife.{Monster, Resource, StoryEvent}
  alias KingdomLife.Location.Plumery

  @behaviour Monster

  @gear []

  @icon "8"

  @innovations []

  @name "Phoenix"

  @resources [
    %Resource{type: :basic, name: "Bird Beak", tags: [:bone]},
    %Resource{type: :basic, name: "Black Skull", tags: [:iron, :skull, :bone]},
    %Resource{type: :basic, name: "Hollow Wing Bones", tags: [:bone]},
    %Resource{type: :basic, name: "Muculent Droppings", tags: [:organ]},
    %Resource{type: :basic, name: "Phoenix Eye", tags: [:organ, :scrap]},
    %Resource{type: :basic, name: "Phoenix Finger", tags: [:bone]},
    %Resource{type: :basic, name: "Phoenix Whisker", tags: [:hide]},
    %Resource{type: :basic, name: "Pustules", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Rainbow Droppings", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Shimmering Halo", tags: [:organ]},
    %Resource{type: :basic, name: "Small Feathers", tags: [:hide]},
    %Resource{type: :basic, name: "Small Hand Parasites", tags: [:organ]},
    %Resource{type: :basic, name: "Tail Feathers", tags: [:hide]},
    %Resource{type: :basic, name: "Wishbone", tags: [:bone]}
  ]

  @story_events [
    %StoryEvent{year_index: 7, title: "Phoenix Feather", type: :story}
  ]

  @strange_resources [
    %Resource{type: :strange, name: "Phoenix Crest", tags: [:organ]}
  ]

  @monster_type :quarry

  @impl Monster
  def gear_sets(_), do: @gear
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(version), do: [Plumery.details(version)]
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
