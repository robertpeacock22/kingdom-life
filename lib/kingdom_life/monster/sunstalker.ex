defmodule KingdomLife.Monster.Sunstalker do
  alias KingdomLife.{Gear, GearSet, Innovation, Monster, Resource, StoryEvent}
  alias KingdomLife.Location.SkyReefSanctuary

  @behaviour Monster

  @icon "q"

  @innovations [
    %Innovation{name: "Shadow Dancing", type: :innovation, status: :unknown}
  ]

  @name "Sunstalker"

  @resources [
    %Resource{type: :basic, name: "Black Lens", tags: [:organ]},
    %Resource{type: :basic, name: "Brain Root", tags: [:organ]},
    %Resource{type: :basic, name: "Cycloid Scales", tags: [:hide]},
    %Resource{type: :basic, name: "Fertility Tentacle", tags: [:organ]},
    %Resource{type: :basic, name: "Huge Sunteeth", tags: [:bone]},
    %Resource{type: :basic, name: "Inner Shadow Skin", tags: [:hide]},
    %Resource{type: :basic, name: "Prismatic Gills", tags: [:organ]},
    %Resource{type: :basic, name: "Shadow Ink Gland", tags: [:organ]},
    %Resource{type: :basic, name: "Shadow Tentacles", tags: [:organ, :hide]},
    %Resource{type: :basic, name: "Shark Tongue", tags: [:organ]},
    %Resource{type: :basic, name: "Small Sunteeth", tags: [:bone]},
    %Resource{type: :basic, name: "Stink Lung", tags: [:organ]},
    %Resource{type: :basic, name: "Sunshark Blubber", tags: [:organ]},
    %Resource{type: :basic, name: "Sunshark Bone", tags: [:bone]},
    %Resource{type: :basic, name: "Sunshark Fin", tags: [:bone, :hide]}
  ]

  @strange_resources [
    %Resource{type: :strange, name: "1,000 Year Sunspot", tags: [:bone, :organ]},
    %Resource{type: :strange, name: "3,000 Year Sunspot", tags: [:bone, :organ, :scrap]},
    %Resource{type: :strange, name: "Bugfish", tags: [:fish, :organ]},
    %Resource{type: :strange, name: "Hagfish", tags: [:fish, :bone, :hide]},
    %Resource{type: :strange, name: "Jowls", tags: [:fish, :iron]},
    %Resource{type: :strange, name: "Salt", tags: []},
    %Resource{type: :strange, name: "Sunstones", tags: [:bone]}
  ]

  @story_events [
    %StoryEvent{year_index: 8, title: "Promise Under the Sun", type: :story}
  ]

  @rare_gear [
    %Gear{count: 1, type: :rare, name: "Eye Patch"},
    %Gear{count: 1, type: :rare, name: "God’s String"}
  ]

  @monster_type :quarry

  @impl Monster
  def gear_sets(_) do
    [
      %GearSet{
        name: "Rare Gear",
        gear: @rare_gear
      }
    ]
  end
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(version), do: [SkyReefSanctuary.details(version)]
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
