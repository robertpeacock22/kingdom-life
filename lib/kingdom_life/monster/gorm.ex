defmodule KingdomLife.Monster.Gorm do
  alias KingdomLife.{Innovation, Monster, Resource, StoryEvent}
  alias KingdomLife.Location.{Gormchymist, Gormery}

  @behaviour Monster

  @icon "l"

  @innovations [
    %Innovation{name: "Albedo", type: :innovation, status: :unknown},
    %Innovation{name: "Citrinitas", type: :innovation, status: :unknown},
    %Innovation{name: "Nigredo", type: :innovation, status: :unknown},
    %Innovation{name: "Rubedo", type: :innovation, status: :unknown}
  ]

  @name "Gorm"

  @resources [
    %Resource{type: :basic, name: "Acid Gland", tags: [:organ]},
    %Resource{type: :basic, name: "Dense Bone", tags: [:bone]},
    %Resource{type: :basic, name: "Gorm Brain", tags: [:organ]},
    %Resource{type: :basic, name: "Handed Skull", tags: [:bone]},
    %Resource{type: :basic, name: "Jiggling Lard", tags: [:organ, :hide]},
    %Resource{type: :basic, name: "Mammoth Hand", tags: [:bone, :hide, :organ]},
    %Resource{type: :basic, name: "Meaty Rib", tags: [:bone, :organ]},
    %Resource{type: :basic, name: "Milky Eye", tags: [:organ]},
    %Resource{type: :basic, name: "Stout Heart", tags: [:organ]},
    %Resource{type: :basic, name: "Stout Hide", tags: [:hide]},
    %Resource{type: :basic, name: "Stout Kidney", tags: [:organ]},
    %Resource{type: :basic, name: "Stout Vertebrae", tags: [:bone]}
  ]

  @strange_resources [
    %Resource{type: :strange, name: "Active Thyroid", tags: [:organ, :consumable]},
    %Resource{type: :strange, name: "Gormite", tags: [:scrap, :iron]},
    %Resource{type: :strange, name: "Pure Bulb", tags: [:organ]},
    %Resource{type: :strange, name: "Stomach Lining", tags: [:organ]}
  ]

  @story_events [
    %StoryEvent{year_index: 1, title: "The Approaching Storm", type: :story}
  ]

  @gear [

  ]

  @monster_type :quarry

  @impl Monster
  def gear_sets(_), do: @gear
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(version), do: [
    Gormchymist.details(version),
    Gormery.details(version)
  ]
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
