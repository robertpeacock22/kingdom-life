defmodule KingdomLife.Monster.Butcher do
  alias KingdomLife.{Gear, GearSet, Monster, Settlement, StoryEvent}
  alias KingdomLife.Campaign.{Stars, Sun}

  @behaviour Monster

  @rare_gear [
    %Gear{count: 1, type: :rare, name: "Butcher's Cleaver (L)"},
    %Gear{count: 1, type: :rare, name: "Butcher's Cleaver (R)"},
    %Gear{count: 1, type: :rare, name: "Forsaker Mask"}
  ]

  @icon "2"

  @innovations []

  @name "The Butcher"

  @resources []

  @strange_resources []

  @story_events %{
    v1_0: [
      %StoryEvent{year_index: 4, title: "Nemesis - Butcher Lvl 1", type: :nemesis}
    ],
    v1_5: [
      %StoryEvent{year_index: 16, title: "Nemesis - Butcher Lvl 2", type: :nemesis},
      %StoryEvent{year_index: 23, title: "Nemesis - Butcher Lvl 3", type: :nemesis}
    ]
  }

  @monster_type :nemesis

  @impl Monster
  def gear_sets(_) do
    [
      %GearSet{
        name: "Rare Gear",
        gear: @rare_gear
      }
    ]
  end
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(_), do: []
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(%Settlement{campaign: Stars}), do: [] # PotStars defines its own Nemesis Encounters
  def story_events(%Settlement{campaign: Sun}) do
    [
      %StoryEvent{year_index: 22, title: "Nemesis - Butcher Level 3", type: :story}
    ]
  end
  def story_events(%Settlement{version: 1.0}), do: @story_events.v1_0
  def story_events(%Settlement{version: 1.5}), do: @story_events.v1_0 ++ @story_events.v1_5
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
