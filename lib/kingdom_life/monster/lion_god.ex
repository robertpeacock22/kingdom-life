defmodule KingdomLife.Monster.LionGod do
  alias KingdomLife.{Gear, GearSet, Innovation, Monster, Resource, StoryEvent}

  @behaviour Monster

  @icon "["

  @innovations [
    %Innovation{name: "The Knowledge Worm", type: :innovation, status: :unknown}
  ]

  @name "Lion God"

  @resources []

  @strange_resources [
    %Resource{type: :strange, name: "Canopic Jar", tags: [:organ, :scrap]},
    %Resource{type: :strange, name: "Old Blue Box", tags: [:scrap]},
    %Resource{type: :strange, name: "Sarcophagus", tags: [:iron]},
    %Resource{type: :strange, name: "Silver Urn", tags: [:bone, :scrap]},
    %Resource{type: :strange, name: "Triptych", tags: [:hide, :scrap]}
  ]

  @story_events [
    %StoryEvent{year_index: 13, title: "The Silver City", type: :story}
  ]

  @rare_gear [
    %Gear{count: 1, type: :rare, name: "Ancient Lion Claws"},
    %Gear{count: 1, type: :rare, name: "Bone Witch Mehndi"},
    %Gear{count: 1, type: :rare, name: "Butcher’s Blood"},
    %Gear{count: 1, type: :rare, name: "Death Mehndi"},
    %Gear{count: 1, type: :rare, name: "Glyph of Solitude"},
    %Gear{count: 1, type: :rare, name: "Golden Plate"},
    %Gear{count: 1, type: :rare, name: "Lantern Mehndi"},
    %Gear{count: 1, type: :rare, name: "Lion God Statue"},
    %Gear{count: 1, type: :rare, name: "Necromancer’s Eye"}
  ]

  @monster_type :quarry

  @impl Monster
  def gear_sets(_) do
    [
      %GearSet{
        name: "Rare Gear",
        gear: @rare_gear
      }
    ]
  end
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(_), do: []
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
