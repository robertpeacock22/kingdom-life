defmodule KingdomLife.Monster.Spidicules do
  alias KingdomLife.{Gear, GearSet, Innovation, Monster, Resource, Settlement, StoryEvent}
  alias KingdomLife.Monster.ScreamingAntelope
  alias KingdomLife.Location.SilkMill

  @behaviour Monster

  @icon "U"

  @innovations [
    %Innovation{name: "Choreia", type: :innovation, status: :unknown},
    %Innovation{name: "Legless Ball", type: :innovation, status: :unknown},
    %Innovation{name: "Silk-Refining", type: :innovation, status: :unknown}
  ]

  @name "Spidicules"

  @resources [
    %Resource{type: :basic, name: "Arachnid Heart", tags: [:organ]},
    %Resource{type: :basic, name: "Chitin", tags: [:hide]},
    %Resource{type: :basic, name: "Exoskeleton", tags: [:hide]},
    %Resource{type: :basic, name: "Eyeballs", tags: [:organ]},
    %Resource{type: :basic, name: "Large Appendage", tags: [:bone]},
    %Resource{type: :basic, name: "Serrated Fangs", tags: [:bone]},
    %Resource{type: :basic, name: "Small Appendages", tags: [:hide]},
    %Resource{type: :basic, name: "Spinnerets", tags: [:organ, :scrap]},
    %Resource{type: :basic, name: "Stomach", tags: [:organ]},
    %Resource{type: :basic, name: "Thick Web Silk", tags: [:silk, :hide]},
    %Resource{type: :basic, name: "Unlaid Eggs", tags: [:organ, :consumable]},
    %Resource{type: :basic, name: "Venom Sack", tags: [:organ, :consumable]}
  ]

  @rare_gear [
    %Gear{count: 1, type: :rare, name: "Grinning Visage"},
    %Gear{count: 1, type: :rare, name: "The Weaver"},
  ]

  @monster_type :quarry

  @strange_resources [
    %Resource{type: :strange, name: "Silken Nervous System", tags: [:organ]},
    %Resource{type: :strange, name: "Web Silk", tags: [:silk]}
  ]

  @story_events [
    %StoryEvent{year_index: 2, title: "Young Rivals", type: :story}
  ]

  defp remove_screaming_antelope(%Settlement{} = settlement) do
    pruned_monsters =
      settlement.monsters
      |> Enum.reject(fn monster -> monster.name == ScreamingAntelope.name() end)

    settlement |> Map.put(:monsters, pruned_monsters)
  end

  @impl Monster
  def gear_sets(_) do
    [
      %GearSet{
        name: "Rare Gear",
        gear: @rare_gear
      }
    ]
  end
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(version), do: [SilkMill.details(version)]
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &remove_screaming_antelope/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
