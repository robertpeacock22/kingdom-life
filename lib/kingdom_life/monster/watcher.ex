defmodule KingdomLife.Monster.Watcher do
  alias KingdomLife.{Monster, StoryEvent}
  alias KingdomLife.Location.ExhaustedLanternHoard

  @behaviour Monster

  @icon ","

  @innovations []

  @locations %{
    v1_5: [
      ExhaustedLanternHoard.details(1.5)
    ]
  }

  @name "The Watcher"

  @resources []

  @strange_resources []

  @story_events [
    %StoryEvent{year_index: 20, title: "Watched", type: :story},
    %StoryEvent{year_index: 25, title: "Nemesis - Watcher", type: :nemesis}
  ]

  @monster_type :nemesis

  @impl Monster
  def gear_sets(_), do: []
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(1.0), do: []
  def locations(1.5), do: @locations.v1_5
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
