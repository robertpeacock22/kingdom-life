defmodule KingdomLife.Monster.DragonKing do
  alias KingdomLife.{Gear, GearSet, Monster, Resource, StoryEvent}
  alias KingdomLife.Location.DragonArmory

  @behaviour Monster

  @icon "P"

  @innovations []

  @name "Dragon King"

  @resources [
    %Resource{type: :basic, name: "Cabled Vein", tags: [:organ]},
    %Resource{type: :basic, name: "Dragon Iron", tags: [:iron]},
    %Resource{type: :basic, name: "Hardened Ribs", tags: [:bone]},
    %Resource{type: :basic, name: "Horn Fragment", tags: [:bone]},
    %Resource{type: :basic, name: "Husk", tags: [:hide]},
    %Resource{type: :basic, name: "King's Claws", tags: [:bone]},
    %Resource{type: :basic, name: "King's Tongue", tags: [:hide]},
    %Resource{type: :basic, name: "Radioactive Dung", tags: [:organ, :scrap]},
    %Resource{type: :basic, name: "Veined Wing", tags: [:hide]}
  ]

  @strange_resources [
    %Resource{type: :strange, name: "Pituitary Gland", tags: [:organ]},
    %Resource{type: :strange, name: "Radiant Heart", tags: [:organ]},
    %Resource{type: :strange, name: "Shining Liver", tags: [:organ]}
  ]

  @story_events [
    %StoryEvent{year_index: 8, title: "Glowing Crater", type: :story}
  ]

  @rare_gear [
    %Gear{count: 2, type: :rare, name: "Celestial Spear"},
    %Gear{count: 2, type: :rare, name: "Dragon Vestments"},
    %Gear{count: 2, type: :rare, name: "Hazmat Shield"},
    %Gear{count: 2, type: :rare, name: "Husk of Destiny"},
    %Gear{count: 2, type: :rare, name: "Regal Edge"}
  ]

  @monster_type :quarry

  @impl Monster
  def gear_sets(_) do
    [
      %GearSet{
        name: "Rare Gear",
        gear: @rare_gear
      }
    ]
  end
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(version), do: [DragonArmory.details(version)]
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
