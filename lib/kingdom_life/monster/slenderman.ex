defmodule KingdomLife.Monster.Slenderman do
  alias KingdomLife.{Gear, GearSet, Innovation, Monster, Resource, Settlement, StoryEvent}
  alias KingdomLife.Monster.KingsMan
  alias KingdomLife.Campaign.Stars

  @behaviour Monster

  @icon "z"

  @innovations [
    %Innovation{name: "Dark Water Research", type: :innovation, status: :unknown}
  ]

  @name "Slenderman"

  @resources []

  @strange_resources [
    %Resource{type: :strange, name: "Crystal Sword Mold", tags: [:hide, :organ, :bone, :consumable]},
    %Resource{type: :strange, name: "Dark Water", tags: [:hide, :organ, :bone, :consumable]}
  ]

  @story_events [
      %StoryEvent{year_index: 6, title: "It's Already Here", type: :story},
  ]

  @gear [
    %Gear{count: 3, type: :gear, name: "Dark Water Vial"},
    %Gear{count: 3, type: :gear, name: "Gloom Bracelets"},
    %Gear{count: 3, type: :gear, name: "Gloom Cream"},
    %Gear{count: 1, type: :gear, name: "Gloom Hammer"},
    %Gear{count: 3, type: :gear, name: "Gloom Katana"},
    %Gear{count: 3, type: :gear, name: "Gloom Mehndi"},
    %Gear{count: 3, type: :gear, name: "Gloom Sheath"},
    %Gear{count: 3, type: :gear, name: "Gloom-Coated Arrow"},
    %Gear{count: 3, type: :gear, name: "Raptor-Worm Collar"},
    %Gear{count: 1, type: :gear, name: "Slender Ovule"}
  ]

  @monster_type :nemesis

  defp remove_kings_man(%Settlement{} = settlement) do
    find_kings_man = fn monster -> monster.name == KingsMan.name() end

    kings_man = Monster.details(KingsMan, settlement)

    neutralized_story_events =
      kings_man.story_events
      |> Enum.filter(fn event -> String.contains?(event.title, "King's Man") end)
      |> Enum.map(fn event -> %StoryEvent{event | title: String.replace(event.title, "King's Man ", "")} end)

    story_events = settlement.story_events ++ neutralized_story_events

    pruned_monsters =
      settlement.monsters
      |> Enum.reject(&(find_kings_man).(&1))

    %Settlement{settlement | monsters: pruned_monsters, story_events: story_events}
  end

  @impl Monster
  def gear_sets(_) do
    [
      %GearSet{
        name: "Dark Water Gear",
        gear: @gear
      }
    ]
  end
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(_), do: []
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &remove_kings_man/1
  @impl Monster
  def story_events(%Settlement{campaign: Stars}), do: [] # PotStars defines its own Nemesis Encounters
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
