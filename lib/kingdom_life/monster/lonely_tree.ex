defmodule KingdomLife.Monster.LonelyTree do
  alias KingdomLife.{Monster, Resource}

  @behaviour Monster

  @gear []

  @icon "~"

  @innovations []

  @name "Lonely Tree"

  @resources []

  @strange_resources [
    %Resource{type: :strange, name: "Blistering Plasma Fruit", tags: [:organ, :consumable]},
    %Resource{type: :strange, name: "Drifting Dream Fruit", tags: [:consumable]},
    %Resource{type: :strange, name: "Jagged Marrow Fruit", tags: [:bone, :scrap, :consumable]},
    %Resource{type: :strange, name: "Lonely Fruit", tags: [:consumable]},
    %Resource{type: :strange, name: "Porous Flesh Fruit", tags: [:hide, :consumable]}
  ]

  @story_events []

  @monster_type :special

  @impl Monster
  def gear_sets(_), do: @gear
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(_), do: []
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
