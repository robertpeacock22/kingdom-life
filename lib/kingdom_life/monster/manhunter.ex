defmodule KingdomLife.Monster.Manhunter do
  alias KingdomLife.{Gear, GearSet, Innovation, Monster, Resource, StoryEvent}

  @behaviour Monster

  @icon "1"

  @innovations [
    %Innovation{name: "Crimson Candy", type: :innovation, status: :unknown},
    %Innovation{name: "Settlement Watch", type: :innovation, status: :unknown},
    %Innovation{name: "War Room", type: :innovation, status: :unknown}
  ]

  @name "Manhunter"

  @resources []

  @strange_resources [
    %Resource{type: :strange, name: "Crimson Vial", tags: [:hide, :organ, :bone, :consumable]},
    %Resource{type: :strange, name: "Red Vial", tags: [:hide, :organ, :bone, :consumable]}
  ]

  @story_events [
    %StoryEvent{year_index: 5, title: "The Hanged Man", type: :story},
    %StoryEvent{year_index: 10, title: "Special Showdown - Manhunter", type: :nemesis},
    %StoryEvent{year_index: 16, title: "Special Showdown - Manhunter", type: :nemesis},
    %StoryEvent{year_index: 22, title: "Special Showdown - Manhunter", type: :nemesis}
  ]

  @gear [
    %Gear{count: 1, type: :gear, name: "Hunter’s Heart"},
    %Gear{count: 1, type: :gear, name: "Tool Belt"},
    %Gear{count: 1, type: :gear, name: "Manhunter’s Hat"},
    %Gear{count: 1, type: :gear, name: "Deathpact"},
    %Gear{count: 1, type: :gear, name: "Reverberating Lantern"}
  ]

  @monster_type :nemesis

  @impl Monster
  def gear_sets(_) do
    [
      %GearSet{
        name: "Manhunter Gear",
        gear: @gear
      }
    ]
  end
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(_), do: []
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
