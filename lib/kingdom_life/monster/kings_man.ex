defmodule KingdomLife.Monster.KingsMan do
  alias KingdomLife.{Gear, GearSet, Innovation, Monster, Settlement, StoryEvent}
  alias KingdomLife.Campaign.{Stars, Sun}

  @behaviour Monster

  @rare_gear [
    # Can be obtained through Guidepost, but Guidepost can only be obtained through King's Man
    %Gear{count: 1, type: :rare, name: "Lantern Halberd"},
    %Gear{count: 2, type: :rare, name: "Regal Faulds"},
    %Gear{count: 2, type: :rare, name: "Regal Gauntlet"},
    %Gear{count: 2, type: :rare, name: "Regal Greaves"},
    %Gear{count: 2, type: :rare, name: "Regal Helm"},
    %Gear{count: 2, type: :rare, name: "Regal Plackart"}
  ]

  @icon "d"

  @innovations [
    %Innovation{name: "Guidepost", type: :innovation, status: :unknown}
  ]

  @name "King's Man"

  @resources []

  @strange_resources []

  @story_events %{
    v1_0: [
      %StoryEvent{year_index: 6, title: "Armored Strangers", type: :story},
      %StoryEvent{year_index: 9, title: "Nemesis - King's Man Lvl 1", type: :nemesis}
    ],
    v1_5: [
      %StoryEvent{year_index: 19, title: "Nemesis - King's Man Lvl 2", type: :nemesis},
      %StoryEvent{year_index: 28, title: "Nemesis - King's Man Lvl 3", type: :nemesis}
    ]
  }

  @monster_type :nemesis

  @impl Monster
  def gear_sets(_) do
    [
      %GearSet{
        name: "Rare Gear",
        gear: @rare_gear
      }
    ]
  end
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(_), do: []
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(%Settlement{campaign: Stars}), do: [] # PotStars defines its own Nemesis Encounters
  def story_events(%Settlement{campaign: Sun}) do
    [
      %StoryEvent{year_index: 21, title: "Nemesis - King's Man Level 2", type: :story},
      %StoryEvent{year_index: 23, title: "Nemesis - King's Man Level 3", type: :story}
    ]
  end
  def story_events(%Settlement{version: 1.0}), do: @story_events.v1_0
  def story_events(%Settlement{version: 1.5}), do: @story_events.v1_0 ++ @story_events.v1_5
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
