defmodule KingdomLife.Monster.Hand do
  alias KingdomLife.{Monster, Settlement, StoryEvent}
  alias KingdomLife.Campaign.{Stars, Sun}

  @behaviour Monster

  @gear []

  @icon "a"

  @innovations []

  @name "The Hand"

  @resources []

  @strange_resources []

  @story_events [
    %StoryEvent{year_index: 11, title: "Regal Visit", type: :story},
    %StoryEvent{year_index: 13, title: "Nemesis - The Hand Lvl 1", type: :nemesis}
  ]

  @monster_type :nemesis

  @impl Monster
  def gear_sets(_), do: @gear
  @impl Monster
  def icon, do: @icon
  @impl Monster
  def innovations, do: @innovations
  @impl Monster
  def locations(_), do: []
  @impl Monster
  def name, do: @name
  @impl Monster
  def resources, do: @resources
  @impl Monster
  def settlement_modifier(), do: &Monster.noop/1
  @impl Monster
  def story_events(%Settlement{campaign: Stars}), do: [] # PotStars defines its own Nemesis Encounters
  def story_events(%Settlement{campaign: Sun}) do
    [
      %StoryEvent{year_index: 24, title: "Nemesis - The Hand Level 3", type: :story}
    ]
  end
  def story_events(_), do: @story_events
  @impl Monster
  def strange_resources, do: @strange_resources
  @impl Monster
  def type, do: @monster_type
end
