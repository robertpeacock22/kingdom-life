defmodule KingdomLife.Resource do
  @moduledoc """
  A Struct defining a resource.

  This includes information about what it can be used for, but only enough
  to provide an at-a-glance overview.
  """

  use TypedStruct

  typedstruct do
    field :name, String.t(), enforce: true
    field :tags, [atom()], enforce: true
    field :type, atom(), enforce: true
  end
end
