defmodule KingdomLife.Services.Settlement do

  alias KingdomLife.{GearSet, LanternYear, Monster, Settlement}
  alias KingdomLife.Campaign.{Bloom, Lantern, Stars, Sun}

  # The order of this mapping determines the order in which each monster is
  # evaluated for addition – hence the lack of alpha-sort!
  @module_mapping %{
    v1_0: [
      white_lion: Monster.WhiteLion,
      screaming_antelope: Monster.ScreamingAntelope,
      phoenix: Monster.Phoenix,
      butcher: Monster.Butcher,
      kings_man: Monster.KingsMan,
      hand: Monster.Hand,
      watcher: Monster.Watcher,
      dung_beetle_knight: Monster.Knight.DungBeetle,
      flower_knight: Monster.Knight.Flower,
      lion_knight: Monster.Knight.Lion,
      dragon_king: Monster.DragonKing,
      gorm: Monster.Gorm,
      herald_of_death: Monster.HeraldOfDeath,
      lion_god: Monster.LionGod,
      lonely_tree: Monster.LonelyTree,
      manhunter: Monster.Manhunter,
      slenderman: Monster.Slenderman,
      spidicules: Monster.Spidicules,
      sunstalker: Monster.Sunstalker
    ],
    v1_5: [
      gold_smoke_knight: Monster.Knight.GoldSmoke
    ]
  }

  @doc """
  Translate options to Monster modules
  """
  def translate(options) do
      module_mapping = module_mapping(options.version)

      module_mapping
      |> Keyword.keys()
      |> Enum.reduce([], fn monster_key, acc -> add_monster_module(acc, monster_key, options, module_mapping) end)
  end

  defp module_mapping(1.0), do: @module_mapping.v1_0
  defp module_mapping(1.5), do: @module_mapping.v1_0 ++ @module_mapping.v1_5

  @doc """
  Build a Settlement using content from specified Monster modules
  """
  def build(monster_modules, version, campaign) do
    version
    |> Settlement.new()
    |> set_campaign(campaign)
    |> add_innovations()
    |> add_monsters(monster_modules)
    |> add_locations()
    |> add_years()
  end

  def set_campaign(%Settlement{} = settlement, campaign_atom) do
    %Settlement{settlement | campaign: module_for_campaign(campaign_atom)}
  end

  def add_innovations(%Settlement{campaign: campaign} = settlement) do
    settlement
    |> add_base_innovations()
    |> campaign.update_innovations()
  end

  def add_locations(%Settlement{campaign: campaign} = settlement) do
    settlement
    |> campaign.update_locations()
  end

  defp add_base_innovations(%Settlement{version: version} = settlement) do
    innovations_to_add =
      version
      |> Settlement.innovations_for_version()
      |> Enum.sort(&(&1.name < &2.name))

    %Settlement{settlement | innovations: innovations_to_add}
  end

  defp module_for_campaign(:bloom), do: Bloom
  defp module_for_campaign(:lantern), do: Lantern
  defp module_for_campaign(:stars), do: Stars
  defp module_for_campaign(:sun), do: Sun

  @doc """
  Add a Monster to a Settlement.

  Public for testing purposes – this may change.
  """
  def add_monster(%Settlement{monsters: monsters} = settlement, %Monster{} = new_monster) do
    if Enum.member?(monsters, new_monster), do: raise(RuntimeError, "Cannot add the same monster twice")

    settlement
    |> Map.put(:monsters, monsters ++ [new_monster])
    |> new_monster.settlement_modifier.()
  end

  @doc """
  List all resources that a Settlement will potentially have access to.
  """
  def list_resources(%Settlement{monsters: monsters, resources: resources}) do
    monsters
    |> Enum.reduce(resources, fn monster, acc ->
      acc ++ [monster.resource_set]
    end)
  end

  @doc """
  List all gear that a Settlement will potentially have access to.
  """
  def list_gear(%Settlement{gear_sets: gear_sets, monsters: monsters}) do
    monsters
    |> Enum.reduce(gear_sets, fn monster, acc ->
      location_sets =
        monster.locations
        |> Enum.reduce([], fn location, acc -> acc ++ [location.gear_set] end)
      acc ++ location_sets ++ monster.gear_sets
    end)
    |> consolidate_rare_gear()
  end

  defp consolidate_rare_gear(gear_sets) do
    {rare, normal} =
      gear_sets
      |> Enum.split_with(fn gear_set -> gear_set.name == "Rare Gear" end)

    gear_from_rare_sets =
      rare
      |> Enum.reduce([], fn gear_set, acc ->
        acc ++ gear_set.gear
      end)
      |> Enum.sort(&(&1.name < &2.name))

    rare_gear_set = %GearSet{
      name: "Rare Gear",
      gear: gear_from_rare_sets
    }

    normal
    |> List.insert_at(1, rare_gear_set)
  end

  @doc """
  List all innovations that a Settlement will potentially have access to.
  """
  def list_innovations(%Settlement{innovations: innovations, monsters: monsters}) do
    monsters
    |> Enum.reduce(innovations, fn monster, acc ->
      acc ++ monster.innovations
    end)
  end

  @doc """
  List all weapon_masteries that a Settlement will potentially have access to.
  """
  def list_weapon_masteries(%Settlement{weapon_masteries: weapon_masteries}) do
    weapon_masteries
  end

  @doc """
  List all locations that a Settlement will potentially have access to.
  """
  def list_locations(%Settlement{locations: locations, monsters: monsters}) do
    monsters
    |> Enum.reduce(locations, fn monster, acc ->
      acc ++ monster.locations
    end)
  end

  defp add_monsters(%Settlement{} = settlement, monster_modules) do
    new_monsters = Enum.map(monster_modules, fn monster -> Monster.details(monster, settlement) end)
    Enum.reduce(new_monsters, settlement, fn monster, acc ->
      add_monster(acc, monster)
    end)
  end

  defp add_monster_module(monsters, current, options, module_mapping) do
    if options[current] do
      monsters ++ [Keyword.get(module_mapping, current)]
    else
      monsters
    end
  end

  defp add_years(%Settlement{campaign: campaign} = settlement) do
    settlement
    |> add_base_years(1..36)
    |> add_story_events()
    |> campaign.add_story_events()
    |> campaign.check_monsters()
    |> add_monster_stories()
  end

  defp add_story_events(%Settlement{story_events: story_events, years: years} = settlement) do
    updated_years =
      story_events
      |> Enum.reduce(years, &add_story_event_to_years/2)

      %Settlement{settlement | years: updated_years}
  end

  defp add_base_years(%Settlement{} = settlement, range) do
    years = Enum.reduce(range, [], fn year_index, acc  -> acc ++ [LanternYear.new(year_index)] end)
    %Settlement{settlement | years: years}
  end

  defp add_monster_stories(%Settlement{monsters: monsters, years: years} = settlement) do
    updated_years =
      monsters
      |> Enum.reduce([], fn monster, acc ->
        acc ++ monster.story_events
      end)
      |> Enum.reduce(years, &add_story_event_to_years/2)

    %Settlement{settlement | years: updated_years}
  end

  def add_story_event_to_years(story_event, years) do
    year = Enum.at(years, story_event.year_index - 1)
    updated_year =
      year
      |> Map.put(:story_events, year.story_events ++ [story_event])

    List.replace_at(years, story_event.year_index - 1, updated_year)
  end
end
