defmodule KingdomLife.Services.PDF do
  alias KingdomLife.{Gear, Innovation, Location, Resource, Settlement}
  alias KingdomLife.Services.Settlement, as: SettlementService
  alias KingdomLife.Services.PDF.Font

  @story_symbol ">"
  @nemesis_symbol "4"

  @rows_per_page 50 # Trial-and-error for now
  @columns_per_page 4

  defmodule SublistHeader do
    use TypedStruct

    typedstruct do
      field :title, String.t(), enforce: true
      field :icon, String.t(), enforce: true
    end
  end

  def generate_pdf(%Settlement{} = settlement) do
    {:ok, binary_pdf_content} =
      settlement
      |> static_html()
      |> PdfGenerator.generate_binary(page_size: "LETTER", shell_params: ["--print-media-type"])

    binary_pdf_content
  end

  defp render_resources_and_gear(settlement) do
    resources =
      settlement
      |> SettlementService.list_resources()
      |> Enum.reject(fn resource_set -> length(resource_set.resources) == 0 end)
      |> Enum.reduce([], fn resource_set, acc ->
        header = %SublistHeader{title: resource_set.name, icon: resource_set.icon}
        acc ++ [header] ++ resource_set.resources
      end)

    gear =
      settlement
      |> SettlementService.list_gear()
      |> Enum.reduce([], fn gear_set, acc ->
        header = %SublistHeader{title: gear_set.name, icon: "a"}
        acc ++ [header] ++ gear_set.gear
      end)

    pages = resources_and_gear_to_pages(resources, gear)

    pages
    |> Enum.map(fn page_rows -> "<div class=\"new-page\">#{page_rows}</div>" end)
    |> Enum.join()
  end

  defp title_row(title) do
    "<tr>
      <td style=\"background-color: #222222\" colspan=\"#{@columns_per_page}\">
        <font style=\"color: #FFFFFF\">
          <strong>
            #{title}
          </strong>
        </font>
      </td>
    </tr>"
  end

  defp resources_and_gear_to_pages(resources, gear) do
    resources_title_row = title_row("RESOURCES")
    gear_title_row = title_row("GEAR")

    [[]] # List of pages, of which there is currently one (empty)
    |> add_title_row(resources_title_row)
    |> fill_from_list(resources)
    |> add_title_row(gear_title_row)
    |> fill_from_list(gear)
    |> Enum.map(fn page -> rows_to_table(page) end)
  end

  defp fill_from_list(pages, fill_list) do
    current_page = List.last(pages)

    cond do
      length(fill_list) == 0 ->
        # Nothing left to fill.
        pages
      length(current_page) == @rows_per_page ->
        # Current page is full. Start a new page.
        fill_from_list(pages ++ [[]], fill_list)
      true ->
        num_rows_to_fill = @rows_per_page - length(current_page)

        {items_to_fill_with, rest} = Enum.split(fill_list, num_rows_to_fill * @columns_per_page)

        max_items_per_column = column_height(items_to_fill_with, @columns_per_page)

        rendered_items =
          items_to_fill_with
          |> render_items_to_rows(max_items_per_column)
          |> wrap_rows()

        new_pages = List.replace_at(pages, length(pages) - 1, List.last(pages) ++ rendered_items)

        fill_from_list(new_pages, rest)
    end
  end

  defp add_title_row(pages, title_row) do
    current_page = List.last(pages)

    if length(current_page) in (@rows_per_page - 1)..@rows_per_page do
      # Current page is full (or very nearly full). Start a new page.
      pages ++ [title_row]
    else
      # Add the title row to the current last page
      List.replace_at(pages, length(pages) -1, List.last(pages) ++ [title_row])
    end
  end

  defp render_table(item_list, rows_per_page, num_columns) do
    items_per_page = rows_per_page * num_columns
    if Enum.count(item_list) >= items_per_page do
      # There is a full page's worth
      {this_page, rest} = Enum.split(item_list, items_per_page)

      rendered_rest = if length(rest) > 0 do
        render_table(rest, rows_per_page, num_columns)
      else
        []
      end

      render_items_to_rows(this_page, rows_per_page) ++ rendered_rest
    else
      # There is less than a full page's worth
      max_items_per_column = column_height(item_list, num_columns)

      render_items_to_rows(item_list, max_items_per_column)
    end
  end

  defp render_items_to_rows(item_list, num_rows) do
    item_list
    |> Enum.map(&render_cell/1)
    |> Enum.chunk_every(num_rows)
    |> possibly_empty_cells()
  end

  defp column_height(cell_list, num_columns) do
    cell_list
    |> length()
    |> Kernel./(num_columns)
    |> :math.ceil()
    |> Kernel.trunc()
    |> max(1)
  end

  defp possibly_empty_cells(chunked_cells) do
    1..(List.first(chunked_cells) |> Enum.count())
    |> Enum.reduce([], fn index, acc ->
      row =
        chunked_cells
        |> Enum.map(fn col ->
          if length(col) >= index do
            Enum.at(col, index - 1)
          else
            :drop
          end
        end)
        |> Enum.map(fn x ->
          case x do
            :drop -> "<td></td>"
            otherwise -> otherwise
          end
        end)

      acc ++ [row]
    end)
  end

  defp wrap_rows(row_list) do
    row_list
    |> Enum.map(fn row -> "<tr>#{row}</tr>" end)
  end

  defp render_cell(%Location{status: :owned} = location), do: "■ #{location.name}"
  defp render_cell(%Location{status: :unclaimed} = location), do: "☐ #{location.name}"
  defp render_cell(%Resource{type: :strange} = resource) do
    "<td bgcolor=\"#DDDDDD\">#{render_cell_content(resource)}</td>"
  end
  defp render_cell(%Resource{} = resource) do
    "<td>#{render_cell_content(resource)}</td>"
  end
  defp render_cell(%Gear{} = gear) do
    "<td>#{render_cell_content(gear)}</td>"
  end
  defp render_cell(%SublistHeader{icon: icon, title: title}) do
    "<td bgcolor=\"#999999\"><font face=\"EVILZ\">#{icon}</font> #{title}</td>"
  end
  defp render_cell(%Innovation{name: name, type: :innovation, status: status}) do
    "<td>#{render_innovation_status(status)} #{name}</td>"
  end
  defp render_cell(%Innovation{name: name, type: :weapon_mastery}) do
    "<td>☐ <font face=\"EVILZ\">$</font> #{name}</td>"
  end

  defp render_innovation_status(:known), do: "■■"
  defp render_innovation_status(:in_deck), do: "■☐"
  defp render_innovation_status(:unknown), do: "☐☐"

  defp render_cell_content(%Resource{name: name, tags: tags}) do
    "<b><font face=\"cascadia-code-mono\" style=\"font-size:10px;\">#{render_tags(tags)}</font></b> _____ #{name}"
  end

  defp render_cell_content(%Gear{name: name}) do
    "_____ • <font style=\"font-size:14px;\">#{name}</font>"
  end

  defp rows_to_table(list) do
    list
    |> Enum.join
    |> (fn rows -> "<table>#{rows}</table>" end).()
  end

  defp render_tags(card_tags) do
    [:bone, :hide, :organ, :consumable]
    |> Enum.reduce([], fn tag, acc ->
      tag_symbol =
        if tag in card_tags do
          letter_for(tag)
        else
          "."
        end

        acc ++ [tag_symbol]
    end)
    |> Enum.join("")
  end

  defp letter_for(tag) do
    tag
    |> Atom.to_string()
    |> String.first()
    |> String.capitalize()
  end

  defp render_years(%Settlement{years: years}) do
    {left, right} =
      years
      |> Enum.reduce([], fn year, acc ->
        story_string =
          year.story_events
          |> render_story_events()
          |> Enum.join(", ")
        acc ++ ["<td style=\"width: 8%\">☐ #{year.index}</td><td style=\"width: 42%\">#{story_string}</td>"]
      end)
      |> add_first_day()
      |> Enum.split(18)

      left
      |> Enum.zip(right)
      |> Enum.map(fn {left, right} -> "<tr>#{left}#{right}</tr>" end)
      |> rows_to_table()
  end

  defp add_first_day(year_strings) do
    first_year =
      year_strings
      |> List.first()
      |> String.replace("<strong>SE</strong>", "<strong>First Day</strong>")

    year_strings
    |> List.replace_at(0, first_year)
  end

  defp render_locations(%Settlement{} = settlement) do
    settlement
    |> SettlementService.list_locations()
    |> Enum.map(fn location -> render_cell(location) end)
    |> render_items_to_rows()
    |> rows_to_table()
  end

  defp render_quarries(%Settlement{monsters: monsters}) do
    monsters
    |> Enum.filter(fn monster -> monster.type == :quarry end)
    |> Enum.map(fn monster -> "☐ #{monster.name}" end)
    |> render_items_to_rows()
    |> rows_to_table()
  end

  defp render_principles(%Settlement{campaign: campaign}) do
    "
    <table>
      <tr><td colspan=\"3\" bgcolor=\"#CCCCCC\">New Life</td></tr>
      #{new_life_principle(campaign)}
      <tr><td colspan=\"3\" bgcolor=\"#CCCCCC\">Death</td></tr>
      <tr><td style=\"width: 40%\">☐ Cannibalize</td><td style=\"width: 10%\"><strong>~or~</strong></td><td style=\"width: 50%\">☐ Graves</td></tr>
      <tr><td colspan=\"3\" bgcolor=\"#CCCCCC\">Society</td></tr>
      <tr><td style=\"width: 40%\">☐ Collective Toil<br /><td style=\"width: 10%\"><strong>~or~</strong></td><td style=\"width: 50%\">☐ Accept Darkness</td></tr>
      <tr><td colspan=\"3\" bgcolor=\"#CCCCCC\">Conviction</td></tr>
      <tr><td style=\"width: 40%\">☐ Barbaric</td><td style=\"width: 10%\"><strong>~or~</strong></td><td style=\"width: 50%\">☐ Romantic</td></tr>
    </table>
    "
  end

  defp new_life_principle(KingdomLife.Campaign.Sun) do
    "<tr><td style=\"width: 40%\"><strike>☐ Protect the young</strike></td><td style=\"width: 10%\"><strong>~or~</strong></td><td style=\"width: 50%\">■ Survival of the Fittest</td></tr>"
  end
  defp new_life_principle(_) do
    "<tr><td style=\"width: 40%\">☐ Protect the young</td><td style=\"width: 10%\"><strong>~or~</strong></td><td style=\"width: 50%\">☐ Survival of the Fittest</td></tr>"
  end

  defp render_nemeses(%Settlement{monsters: monsters}) do
    monsters
    |> Enum.filter(fn monster -> monster.type == :nemesis end)
    |> Enum.map(fn monster -> "☐ #{monster.name}" end)
    |> render_nemeses_to_rows()
    |> rows_to_table()
  end

  defp render_nemeses_to_rows(nemeses) do
    nemeses
    |> Enum.map(fn nemesis -> "<tr><td style=\"width:34%,display:block\">#{nemesis}</td><td style=\"width:12%\">☐ Lvl 1</td><td style=\"width:12%\">☐ Lvl 2</td><td style=\"width:12%\">☐ Lvl 3</td></tr>" end)
  end

  defp render_items_to_rows(items) when length(items) <= 5 do
    items
    |> Enum.map(fn item -> "<tr><td>#{item}</td></tr>" end)
  end
  defp render_items_to_rows(items) do
    items
    |> to_two_columns()
  end

  defp to_two_columns(things) do
    split_at =
      things
      |> Enum.count()
      |> Kernel./(2)
      |> :math.ceil()
      |> Kernel.trunc()

    {left, right} =
      things
      |> Enum.reduce([], fn innovation, acc ->
        acc ++ [innovation]
      end)
      |> Enum.split(split_at)

    render_two_columns(left, right)
  end

  defp render_innovations(%Settlement{} = settlement) do
    rendered_innovations =
      SettlementService.list_innovations(settlement)
      |> Kernel.++(SettlementService.list_weapon_masteries(settlement))
      |> Enum.sort(fn left, right ->
        cond do
          left.type == :innovation && right.type == :weapon_mastery -> true
          left.type == right.type && left.name < right.name -> true
          true -> false
        end
      end)

    split_at =
      rendered_innovations
      |> Enum.count()
      |> Kernel./(3)
      |> :math.ceil()
      |> Kernel.trunc()

    rendered_innovations
    |> render_table(split_at, 3)
    |> add_innovation_headers()
    |> wrap_rows()
    |> rows_to_table()
  end

  defp add_innovation_headers(list_of_rows) do
    ["<tr><td bgcolor=\"#CCCCCC\">□□ - unknown</td><td bgcolor=\"#CCCCCC\">■□ - in deck</td><td bgcolor=\"#CCCCCC\">■■ - known</td></tr>"] ++ list_of_rows
  end

  defp render_two_columns(left, right) do
    left
    |> Enum.zip(right)
    |> Enum.map(fn {a, b} -> "<tr><td>#{a}</td><td>#{b}</td></tr>" end)
  end

  defp render_story_events(story_events) do
    rendered_story_events =
      Enum.map(story_events, fn event ->
        event_symbol =
          case event.type do
            :story -> @story_symbol
            :nemesis -> @nemesis_symbol
          end

        "<font face=\"EVILZ\">#{event_symbol}</font> #{event.title}"
      end)

      ["<strong>SE</strong>"] ++ rendered_story_events
  end

  defp render_survivors(settlement) do
    header_row = ["<tr><td style=\"width:40%\"><strong>Name</strong><td style=\"width:5%\"><strong>Gender</strong></td><td style=\"width:55%\"><strong>Notes</strong></td>"]

    rows =
      1..40 # Trial-and-error for now
      |> Enum.reduce(header_row, fn _index, acc ->
        acc ++ ["<tr><td style=\"width:40%\">&nbsp;</td><td style=\"width:5%\">&nbsp;</td><td style=\"width:55%\">&nbsp;</td>"]
      end)
      |> Enum.join()

    "<span>____ Population • #{settlement.campaign.name()} • Lost Settlements ☐☐☐☐☐ ☐☐☐☐☐ ☐☐☐☐☐ ☐☐☐☐☐</span>
    <br />
    <table id=\"survivors\">#{rows}</table>"
  end

  defp milestone_timing(KingdomLife.Campaign.Sun), do: 8
  defp milestone_timing(_), do: 5

  defp milestone_name(KingdomLife.Campaign.Sun), do: "Edged Tonometry"
  defp milestone_name(_), do: "Hooded Knight"

  defp static_html(%Settlement{campaign: campaign} = settlement) do
    "
    <html>
    <head>
    <meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" /><meta charset=\"UTF-8\" />
    </head>
    <style>#{font_css()}#{table_css()}#{misc_css()}</style>
    <body>
    <div>
      <div class=\"header_row\">
        <div class=\"header_left_column\">
          <div>Survival Limit: ____ (+1 when you first name the settlement)</div>
          <div>Settlement Name: _______________________________________</div>
          #{render_years(settlement)}
        </div>
        <div class=\"header_right_column\">
          Death Count:<br />
          ☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐<br />
          ☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐<br />
          <br />
          <div class=\"content\">
            Milestone Story Events<br />
            Trigger these story events when the milestone condition is met.<br />
            <table>
              <tr><td>☐ First child is born</td></tr>
              <tr><td>&nbsp;↳ <font face=\"EVILZ\">#{@story_symbol}</font> Principle: New Life</td></tr>
              <tr><td>☐ First time death counter is updated</td></tr>
              <tr><td>&nbsp;↳ <font face=\"EVILZ\">#{@story_symbol}</font> Principle: Death</td></tr>
              <tr><td>☐ Population reaches 15</td></tr>
              <tr><td>&nbsp;↳ <font face=\"EVILZ\">#{@story_symbol}</font> Principle: Society</td></tr>
              <tr><td>☐ Settlement has #{milestone_timing(campaign)} innovations</td></tr>
              <tr><td>&nbsp;↳ <font face=\"EVILZ\">#{@story_symbol}</font> #{milestone_name(campaign)}</td></tr>
              <tr><td>☐ Population reaches 0</td></tr>
              <tr><td>&nbsp;↳ <font face=\"EVILZ\">#{@story_symbol}</font> Game Over</td></tr>
            </table>
          </div>
          <div class=\"content\">
            Nemesis Monsters<br />
            #{render_nemeses(settlement)}
          </div>
        </div>
      </div>
      <div class=\"mid_row\">
        <div class=\"mid_left_column\">
          <div class=\"content\">
            Innovations and Weapon Masteries<br />
            #{render_innovations(settlement)}
          </div>
        </div>
        <div class=\"mid_right_column\">
          <div class=\"content\">
            Settlement Locations<br />
            #{render_locations(settlement)}
          </div>
          <div class=\"content\">
            Quarries<br />
            #{render_quarries(settlement)}
          </div>
          <div class=\"content\">
            Principles<br />
            #{render_principles(settlement)}
          </div>
        </div>
      </div>
    </div>
    <div class=\"new-page\">
      #{render_survivors(settlement)}
    </div>
    #{render_resources_and_gear(settlement)}
    </body>
    </html>"
  end

  defp font_css() do
    "
    @font-face {
      font-family: 'EVILZ';
      src: url(data:font/truetype;charset=utf-8;base64,#{Font.evilz()}) format(\"truetype\");
    }
    @font-face {
      font-family: 'cascadia-code-mono';
      src: url(data:font/truetype;charset=utf-8;base64,#{Font.cascadia_code_mono()}) format(\"truetype\");
    }
    "
  end

  defp table_css() do
    "
    td {
      width: 25% ;
    }
    "
  end

  defp misc_css() do
    "
    .header_left_column {
      float: left;
      width: 66.66%;
    }
    .header_right_column {
      float: left;
      width: 33.33%;
    }
    .header_row:after {
      content: \"\";
      display: table;
      clear: both;
    }
    .mid_left_column {
      float: left;
      width: 60%;
    }
    .mid_right_column {
      float: left;
      width: 40%;
    }
    .mid_row:after {
      content: \"\";
      display: table;
      clear: both;
    }
    td.border_bottom {
      border-bottom: 1pt solid black;
      margin-left: 4pt;
      margin-right: 4pt;
      width: 50%;
      height: 20px;
    }
    div.content {
      margin: 4px;
      background-color: #EEEEEE
    }
    div.letter {
      height: 330mm; width: 250mm
    }
    div.new-page {
      page-break-before: always;
    }
    #survivors td, #survivors th {
      border: 1px solid #000000;
      padding: 3px;
    }
    "
  end
end
