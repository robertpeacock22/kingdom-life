defmodule KingdomLife.SettlementServiceTest do
  use ExUnit.Case

  alias KingdomLife.{Monster, ResourceSet, Settlement}
  alias KingdomLife.Services.Settlement, as: SettlementService

  describe "add_monster/2" do
    test "settlement adds monsters" do
      nasty_bug = %Monster{
        name: "Nasty Bug",
        gear: "Hacking Tools",
        resource_set: %ResourceSet{name: "Nasty Bug Resources", resources: MapSet.new(["Debugging Insight"])},
        story_events: [],
        settlement_modifier: &Monster.noop/1
      }

      buggy_settlement =
        new_settlement()
        |> SettlementService.add_monster(nasty_bug)

      assert buggy_settlement.monsters |> Enum.count == 1
      assert buggy_settlement.monsters |> List.first == nasty_bug
    end

    test "settlement does not add monsters multiple times" do
      assert_raise(RuntimeError, "Cannot add the same monster twice", fn ->
        evil_twin = %Monster{
          name: "Gemini",
          gear: "Half of a Whole",
          resource_set: %ResourceSet{name: "Gemini Resources", resources: MapSet.new(["Strange Mirror"])},
          story_events: [],
          settlement_modifier: &Monster.noop/1
        }

        new_settlement()
        |> SettlementService.add_monster(evil_twin)
        |> SettlementService.add_monster(evil_twin)
      end)
    end
  end

  describe "list_resources/1" do
    test "settlement lists default resources" do

      default_resource_set = %ResourceSet{name: "Basic Resources", resources: MapSet.new(["default resource A", "default resource B"])}

      default_resource_names =
        new_settlement()
        |> Map.put(:resources, [default_resource_set])
        |> SettlementService.list_resources()
        |> Enum.reduce([], fn resource_set, acc ->
          acc ++ MapSet.to_list(resource_set.resources)
        end)

        assert default_resource_names == ["default resource A", "default resource B"]
    end

    test "settlement lists Monster resources" do
      default_monster = %Monster{
        name: "No-skin",
        gear: "Pickaxe",
        resource_set: %ResourceSet{name: "No-skin Resources", resources: MapSet.new(["monster resource C"])},
        story_events: [],
        settlement_modifier: &Monster.noop/1
      }

      default_resource_set = %ResourceSet{name: "Basic Resources", resources: MapSet.new(["default resource A", "default resource B"])}

      default_and_monster_resources =
        new_settlement()
        |> Map.put(:resources, [default_resource_set])
        |> SettlementService.add_monster(default_monster)
        |> SettlementService.list_resources()
        |> Enum.reduce([], fn resource_set, acc ->
          acc ++ MapSet.to_list(resource_set.resources)
        end)

        assert default_and_monster_resources == ["default resource A", "default resource B", "monster resource C"]
    end
  end

  defp new_settlement() do
    %Settlement{
      fighting_arts: [],
      gear: [],
      innovations: [],
      principles: [],
      monsters: [],
      resources: [],
      story_events: [],
      years: []
    }
  end
end
