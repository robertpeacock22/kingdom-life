# Kingdom Life

Kingdom Life is a settlement-sheet generator for [Kingdom Death: Monster](http://kingdomdeath.com/). A running version of Kingdom Life is currently available at [tabletop.tools](http://www.tabletop.tools/).

## Installation

### Prerequisites

  * [Elixir](https://elixir-lang.org/) must be installed to run the site and/or the Settlement-generation pipeline
  * [wkhtmltopdf](https://wkhtmltopdf.org/) must be available in your path in order to generate PDFs

### Running locally

`mix phx.server` will run the site and make it available at `localhost:4000`

## License
This project is licensed under the [MIT License](https://choosealicense.com/licenses/mit/) - see the LICENSE file for details
